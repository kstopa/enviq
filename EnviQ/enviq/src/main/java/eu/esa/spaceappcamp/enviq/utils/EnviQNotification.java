package eu.esa.spaceappcamp.enviq.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Location;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

import eu.esa.spaceappcamp.enviq.MainActivity;
import eu.esa.spaceappcamp.enviq.R;
import eu.esa.spaceappcamp.enviq.backend.ReverseLocator;
import eu.esa.spaceappcamp.enviq.calculators.NotificationResourceBuilder;
import eu.esa.spaceappcamp.enviq.models.HealthIndex;
import eu.esa.spaceappcamp.enviq.models.MeasurementParameter;

public class EnviQNotification {

    public static void pushNotification(Context context, List<MeasurementParameter> parameters, HealthIndex index, Location currentLocation) {
        pushNotification(context,
                         parameters,
                         new ReverseLocator(context).reverse(currentLocation),
                         index,
                         currentLocation);
    }

    private static void pushNotification(Context context, List<MeasurementParameter> parameters,Address address,
                                        HealthIndex index, Location location) {
        Log.i(MainActivity.LOG_TAG,"creating push notification");
        if(index == null) {
            Log.e(MainActivity.LOG_TAG,"INDEX IS NULL!!!");
            return;
        }
        NotificationResourceBuilder resourceBuilder = new NotificationResourceBuilder(context);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setSmallIcon(R.drawable.enviq_notification);
        builder.setContentTitle(resourceBuilder.getTitleString(index));
        builder.setAutoCancel(true);
        setContentText(address, index, builder);
        builder.setContentIntent(getPendingIntent(context, parameters, address, index, location));
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(1234, getNotification(context, index, builder, resourceBuilder));
    }

    private static Notification getNotification(Context co, HealthIndex index, NotificationCompat.Builder builder, NotificationResourceBuilder resourceBuilder) {
        if (Build.VERSION.SDK_INT<= Build.VERSION_CODES.JELLY_BEAN) {
            return getNotificationIcs(co.getResources(), index, builder, resourceBuilder);
        } else {
            return getNotificationJellyBean(co, index, builder, resourceBuilder);
        }
    }

    private static Notification getNotificationIcs(Resources resources, HealthIndex index, NotificationCompat.Builder builder, NotificationResourceBuilder resourceBuilder) {
        Notification notification;
        builder.setStyle( getBigPicture(resources, index, resourceBuilder));
        notification = builder.build();
        return notification;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private static Notification getNotificationJellyBean(Context co, HealthIndex index, NotificationCompat.Builder builder, NotificationResourceBuilder resourceBuilder) {
        Notification notification;
        notification = builder.build();
        notification.bigContentView = getNofifyView(co, index, resourceBuilder);
        return notification;
    }

    private static void setContentText(Address address, HealthIndex index, NotificationCompat.Builder mBuilder) {
        if (address != null) {
            mBuilder.setContentText("Measured in " + address.getLocality());
        }
        else {
            mBuilder.setContentText(index.getText());
        }
    }

    private static PendingIntent getPendingIntent(Context co, List<MeasurementParameter> parameters, Address address, HealthIndex index, Location location) {
        Intent intent = buildIntent(co, parameters, address, index, location);
        return PendingIntent.getActivity(co, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
    }

    private static Intent buildIntent(Context co, List<MeasurementParameter> parameters, Address address, HealthIndex index, Location location) {
        Intent intent = new Intent(co, MainActivity.class);
        intent.putExtra(MainActivity.KEY_PARAMETERS, new ArrayList<MeasurementParameter>(parameters));
        intent.putExtra(MainActivity.KEY_CURRENT_ADDRESS, address);
        intent.putExtra(MainActivity.KEY_CURRENT_HEALTHINDEX, index);
        intent.putExtra(MainActivity.KEY_LOCATION, location);
        intent.putExtra(MainActivity.KEY_LATEST_INTENTID, new Date().getTime());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        return intent;
    }

    private static RemoteViews getNofifyView(Context co, HealthIndex index, NotificationResourceBuilder resourceBuilder) {
        RemoteViews customNotifView = new RemoteViews(co.getPackageName(), resourceBuilder.calculateLayoutForImage(index));
        customNotifView.setTextViewText(R.id.notification_Text,index.getLongText());
        customNotifView.setTextViewText(R.id.notification_TextNumber,index.getNumberToString());
        return customNotifView;
    }

    private static NotificationCompat.BigPictureStyle getBigPicture(Resources resources, HealthIndex index, NotificationResourceBuilder resourceBuilder) {
        NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle();
        bigPictureStyle.setBigContentTitle(resourceBuilder.getTitleString(index));
        bigPictureStyle.setSummaryText(index.getText());
        bigPictureStyle.bigPicture(BitmapFactory.decodeResource(resources, resourceBuilder.calculateLocalImageForCalculator(index)));
        return bigPictureStyle;
    }
}
