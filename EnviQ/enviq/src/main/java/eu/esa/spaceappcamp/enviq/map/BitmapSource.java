package eu.esa.spaceappcamp.enviq.map;

import android.graphics.Bitmap;
import android.graphics.Color;

/**
 * Created by adamW on 09.05.14.
 */
public class BitmapSource {

    public static Bitmap getDefaultBitmap() {
        Bitmap bm = Bitmap.createBitmap(5, 5, Bitmap.Config.ARGB_8888);

        bm.setHasAlpha(true);

        for (int i=0;i<5;i++) {
            for (int j=0;j<5;j++) {
                bm.setPixel(i, j, Color.argb(0, 0, 0, 0));
            }
        }

        bm.setPixel(0, 0, Color.argb(255, 255, 0, 0));
        bm.setPixel(1, 1, Color.argb(255, 0, 255, 0));
        bm.setPixel(2, 2, Color.argb(255, 0, 0, 255));
        bm.setPixel(3, 3, Color.argb(255, 255, 0, 0));
        bm.setPixel(4, 4, Color.argb(255, 0, 255, 0));

        return bm;
    }
/*
    public static Bitmap getFromDataManager(LatLngBounds bounds) {
        DataManager dm = new DataManager();

       Bitmap dataImage = dm.getPM25(ounds.southwest.latitude, bounds.southwest.longitude, bounds.northeast.latitude, bounds.northeast.longitude).toBitmap();
       return dataImage;
    }
*/

}

