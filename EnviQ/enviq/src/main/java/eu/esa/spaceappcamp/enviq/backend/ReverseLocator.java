package eu.esa.spaceappcamp.enviq.backend;

import java.io.IOException;
import java.util.List;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.util.Log;

import eu.esa.spaceappcamp.enviq.errorhandling.ErrorAddress;

public class ReverseLocator {
    private static final String LOG_TAG = "EnviQ";
    private final Geocoder geoCoder;
    private final Context context;

    public ReverseLocator(Context context) {
        this.context = context;
        this.geoCoder = new Geocoder(context);
    }

    public Address reverse(Location location) {

        if (geoCoder.isPresent()&& location!= null) {
            try {
                List<Address> addresses = geoCoder.getFromLocation(location.getLatitude(),
                        location.getLongitude(), 1);
                if (addresses != null && !addresses.isEmpty()) {
                    return addresses.get(0);
                }
            } catch (IOException e) {
                e.printStackTrace();
                Log.e(LOG_TAG, e.getMessage());
            }
        }
        return ErrorAddress.create(context,location.getLatitude(),location.getLongitude());

    }
}
