package eu.esa.spaceappcamp.enviq.models;

import java.io.Serializable;

import android.content.Context;

public class HealthIndex implements Serializable {
    private final int index;
    private final String text;
    private final String longText;

    public HealthIndex(int i, String text, String longText) {
        this.index = i;
        this.text = text;
        this.longText = longText;
    }

    public String getNumberToString() {
        if (index<0)
        {
            return "?";
        }
        return String.valueOf(index);
    }

    public int getNumber() {
        return index;
    }

    public String getText() {
        return text;
    }

    public String getLongText() {
        return longText;
    }

    public static  HealthIndex getErrorIndex(Context context)
    {
        return new HealthIndex(-1,"There was an Error","Errors do occur, we are sorry");
    }

}
