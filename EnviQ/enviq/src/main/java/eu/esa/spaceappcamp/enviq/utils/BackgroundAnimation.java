package eu.esa.spaceappcamp.enviq.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Build;
import android.view.View;

/**
 * Created by hannes on 11/05/14.
 */
public class BackgroundAnimation {

    public static void startAnimation(Context co,View rootView, int newBackroundID, int transitiontime) {
        Drawable newBackGround = co.getResources().getDrawable(newBackroundID);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            applyJellybeanBackground(rootView,newBackGround, transitiontime);

        } else {
            applyPreJellyBeanBackground(rootView,newBackGround, transitiontime);
        }
    }


    @SuppressLint("NewApi")
    private static void applyJellybeanBackground(View rootView, Drawable newBackGround, int transitionTime)
    {
        Drawable firstLayer = rootView.getBackground();
        if (firstLayer instanceof TransitionDrawable)
        {
            firstLayer = ((TransitionDrawable) firstLayer).getDrawable(1);
        }
        Drawable[] layers = new Drawable[] { firstLayer, newBackGround };
        TransitionDrawable background = new TransitionDrawable(layers);
        int bottom = rootView.getPaddingBottom();
        int top = rootView.getPaddingTop();
        int right = rootView.getPaddingRight();
        int left = rootView.getPaddingLeft();
        rootView.setBackground(background);
        rootView.setPadding(left, top, right, bottom);
        background.startTransition(transitionTime);
    }

    @SuppressWarnings("deprecation")
    private static void applyPreJellyBeanBackground(View rootView, Drawable newBackGround,int transitionTime)
    {
        Drawable firstLayer = rootView.getBackground();
        if (firstLayer instanceof TransitionDrawable)
        {
            firstLayer = ((TransitionDrawable) firstLayer).getDrawable(1);
        }
        Drawable[] layers = new Drawable[] { firstLayer, newBackGround };
        TransitionDrawable background = new TransitionDrawable(layers);
        int bottom = rootView.getPaddingBottom();
        int top = rootView.getPaddingTop();
        int right = rootView.getPaddingRight();
        int left = rootView.getPaddingLeft();
        rootView.setBackgroundDrawable(newBackGround);
        rootView.setPadding(left, top, right, bottom);
        background.startTransition(transitionTime);

    }


}
