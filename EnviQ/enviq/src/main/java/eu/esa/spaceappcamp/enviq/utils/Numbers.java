package eu.esa.spaceappcamp.enviq.utils;

public final class Numbers {

    public static int max(int... values) {
        int value = 0;
        for(int i=0; i<values.length; i++) {
            value = Math.max(value, values[i]);
        }
        return value;
    }

    public static double convertMicrogramsPerCubicCentimetersIntoMicrogramsPerCubicMeter(double in) {
        return in * 1000000;
    }
}
