package eu.esa.spaceappcamp.enviq.settings;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceGroup;

import eu.esa.spaceappcamp.enviq.R;

public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        updatePreferences();
    }


    private void updatePreferences() {
        for (int i = 0; i < getPreferenceScreen().getPreferenceCount(); ++i) {
            Preference preference = getPreferenceScreen().getPreference(i);
            if (isPreferenceGroup(preference)) {
                updatePreferenceGroup((PreferenceGroup) preference);
            } else {
                updatePreference(preference);
            }
        }
    }

    private void updatePreferenceGroup(PreferenceGroup preferenceGroup) {
        for (int j = 0; j < preferenceGroup.getPreferenceCount(); ++j) {
            updatePreference(preferenceGroup.getPreference(j));
        }
    }

    private boolean isPreferenceGroup(Preference preference) {
        return preference instanceof PreferenceGroup;
    }

    private void updatePreference(Preference preference) {
        if (preference instanceof ListPreference) {
            ListPreference listPreference = (ListPreference) preference;
            listPreference.setSummary(listPreference.getEntry());
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        updatePreference(findPreference(key));
        if (getActivity() != null) {
            getActivity().setResult(Activity.RESULT_OK);
            getActivity().finish();
        }
    }
}
