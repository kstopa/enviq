package eu.esa.spaceappcamp.enviq.calculators;

import android.content.Context;

import eu.esa.spaceappcamp.enviq.R;
import eu.esa.spaceappcamp.enviq.models.Measurement;

import static eu.esa.spaceappcamp.enviq.utils.Numbers.max;

/**
 * http://www.lubw.baden-wuerttemberg.de/servlet/is/20152/
 */
class BadenWuertembergAirIndex extends IndexCalculator {

    public static final int MAX_SULPHUR_DIOXIDE = 1000;
    public static final int MAX_BIG_PARTICLES = 100;
    public static final int MAX_CARBON_MONOXIDE = 30000;
    public static final int MAX_GROUND_LEVEL_OZONE = 240;
    public static final int MAX_NITROGEN_DIOXIDE = 500;
    private final Context context;

    BadenWuertembergAirIndex(Context context) {
        super(context.getString(R.string.indexLuqx));
        this.context = context;
    }

    @Override
    public int calculate(Measurement measurement) {
        int value1 = getNitronDioxideIndex(measurement.getNitrogenDioxide());
        int value2 = getOzonIndex(measurement.getGroundLevelOzone());
        int value3 = getParticleIndex(measurement.getRespirableSuspendedParticle());
        int value4 = getCarbonMonoxideIndex(measurement.getCarbonMonoxide());
        int value5 = getSulphurDioxide(measurement.getSulfurDioxide());
        return max(value1, value2, value3, value4, value5);
    }

    private int getSulphurDioxide(int sulfurDioxide) {
        if (sulfurDioxide <= 25) {
            return 1;
        } else if (sulfurDioxide <= 50) {
            return 2;
        } else if (sulfurDioxide <= 120) {
            return 3;
        } else if (sulfurDioxide <= 350) {
            return 4;
        } else if (sulfurDioxide <= MAX_SULPHUR_DIOXIDE) {
            return 5;
        }
        return 6;
    }

    private int getParticleIndex(int respirableSuspendedParticle) {
        if (respirableSuspendedParticle <= 10) {
            return 1;
        } else if (respirableSuspendedParticle <= 20) {
            return 2;
        } else if (respirableSuspendedParticle <= 35) {
            return 3;
        } else if (respirableSuspendedParticle <= 50) {
            return 4;
        } else if (respirableSuspendedParticle <= MAX_BIG_PARTICLES) {
            return 5;
        }
        return 6;
    }

    private int getCarbonMonoxideIndex(int carbonMonoxide) {
        if (carbonMonoxide <= 1000) {
            return 1;
        } else if (carbonMonoxide <= 2000) {
            return 2;
        } else if (carbonMonoxide <= 4000) {
            return 3;
        } else if (carbonMonoxide <= 10000) {
            return 4;
        } else if (carbonMonoxide <= MAX_CARBON_MONOXIDE) {
            return 5;
        }
        return 6;
    }

    private int getOzonIndex(int groundLevelOzone) {
        if (groundLevelOzone <= 33) {
            return 1;
        } else if (groundLevelOzone <= 65) {
            return 2;
        } else if (groundLevelOzone <= 120) {
            return 3;
        } else if (groundLevelOzone <= 180) {
            return 4;
        } else if (groundLevelOzone <= MAX_GROUND_LEVEL_OZONE) {
            return 5;
        }
        return 6;
    }

    private int getNitronDioxideIndex(int nitrogenDioxide) {
        if (nitrogenDioxide <= 25) {
            return 1;
        } else if (nitrogenDioxide <= 50) {
            return 2;
        } else if (nitrogenDioxide <= 100) {
            return 3;
        } else if (nitrogenDioxide <= 200) {
            return 4;
        } else if (nitrogenDioxide <= MAX_NITROGEN_DIOXIDE) {
            return 5;
        }
        return 6;
    }

    @Override
    public String getAsString(int index) {
        switch(index) {
            case 1:
                return context.getString(R.string.veryGood);
            case 2:
                return context.getString(R.string.good);
            case 3:
                return context.getString(R.string.satisfying);
            case 4:
                return context.getString(R.string.nonsatisfying);
            case 5:
                return context.getString(R.string.bad);
            case 6:
            default:
                return context.getString(R.string.verybad);
        }
    }

    @Override
    public String getAsLongString(int index) {
        switch(index) {
            case 1:
                return context.getString(R.string.luqx_long_one);
            case 2:
                return context.getString(R.string.luqx_long_two);
            case 3:
                return context.getString(R.string.luqx_long_three);
            case 4:
                return context.getString(R.string.luqx_long_four);
            case 5:
                return context.getString(R.string.luqx_long_five);
            case 6:
            default:
                return context.getString(R.string.luqx_long_six);
        }
    }

    @Override
    public boolean needsNitrogenDioxide() {
        return true;
    }

    @Override
    public boolean needsGroundLevelOzone() {
        return true;
    }

    @Override
    public boolean needsFineParticles() {
        return false;
    }

    @Override
    public boolean needsRespirableSuspendedParticle() {
        return true;
    }

    @Override
    public boolean needsSulfurDioxide() {
        return true;
    }

    @Override
    public boolean needsCarbonMonoxide() {
        return true;
    }

    @Override
    public float getMaxForNitrogenDioxide() {
        return MAX_NITROGEN_DIOXIDE;
    }

    @Override
    public float getMaxForGroundLevelOzone() {
        return MAX_GROUND_LEVEL_OZONE;
    }

    @Override
    public float getMaxForFineParticles() {
        return 0; // not supported
    }

    @Override
    public float getMaxForRespirableSuspendedParticle() {
        return MAX_BIG_PARTICLES;
    }

    @Override
    public float getMaxForSulfurDioxide() {
        return MAX_SULPHUR_DIOXIDE;
    }

    @Override
    public float getMaxForCarbonMonoxide() {
        return MAX_CARBON_MONOXIDE;
    }

    @Override
    public int getMaxIndex() {
        return 6;
    }
}
