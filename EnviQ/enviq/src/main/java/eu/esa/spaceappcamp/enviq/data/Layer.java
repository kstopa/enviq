package eu.esa.spaceappcamp.enviq.data;

/**
 * Created by kstopa on 06.05.14.
 */
public class Layer {

    /**
     * Pollutant contained in the layer.
     */
    private Pollutant pollutant;

    /**
     * Requierd forecast. By default closest forecast from now.
     */
    private Forecast forecast;


    /**
     * Creates layer info top get data of given pollutant for today.
     * @param p Pollutant.
     */
    public Layer(Pollutant p) {
        forecast = new Forecast();
        pollutant = p;
    }

    /**
     * Initializes layer to get data for required pollutant and forecast.
     * @param p Required pollutant.
     * @param fc Forecast.
     */
    private Layer(Pollutant p, Forecast fc) {
        pollutant = p;
        forecast =fc;
    }

    /**
     * Return layer name at WDC WCS service based on the pollutant and the required forecast.
     * @return Layer name.
     */
    @Override
    public String toString() {
        return getName();
    }

    /**
     * Return layer name at WDC WCS service based on the pollutant and the required forecast.
     * @return Layer name.
     */
    public String getName() {
            return "macc_" + pollutant.getApiName() + "_conc_" + forecast.toString();
    }

    public Forecast getForecast() {
        return forecast;
    }
}
