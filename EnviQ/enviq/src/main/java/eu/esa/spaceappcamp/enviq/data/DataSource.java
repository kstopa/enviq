package eu.esa.spaceappcamp.enviq.data;

public interface DataSource {

    double getWeightedCarbonMonoxide(double latitude, double longitude) throws NoDataException;
    double getWeightedPM10(double latitude, double longitude) throws NoDataException;
    double getWeightedSulphurDioxide(double latitude, double longitude) throws NoDataException;
    double getWeightedOzone(double latitude, double longitude) throws NoDataException;
    double getWeightedNitrogenDioxide(double latitude, double longitude) throws NoDataException;
    double getWeightedPM25(double latitude, double longitude) throws NoDataException;
}
