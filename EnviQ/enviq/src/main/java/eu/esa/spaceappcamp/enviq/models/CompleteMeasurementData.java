package eu.esa.spaceappcamp.enviq.models;

import java.util.ArrayList;
import java.util.List;

import android.location.Location;

public class CompleteMeasurementData {
    private List<MeasurementParameter> list = new ArrayList<MeasurementParameter>();
    private HealthIndex index;
    private Location location;

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void set(HealthIndex healthIndex) {
        this.index = healthIndex;
    }

    public void add(MeasurementParameter parameter) {
        list.add(parameter);
    }

    public List<MeasurementParameter> getParameters() {
        return new ArrayList<MeasurementParameter>(list);
    }

    public HealthIndex getIndex() {
        return index;
    }

}
