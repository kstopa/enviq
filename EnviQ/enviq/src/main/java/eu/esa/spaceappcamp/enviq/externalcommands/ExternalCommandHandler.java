package eu.esa.spaceappcamp.enviq.externalcommands;

import android.location.Location;
import android.util.Log;

import de.greenrobot.event.EventBus;
import eu.esa.spaceappcamp.enviq.MainActivity;
import eu.esa.spaceappcamp.enviq.models.Measurement;

/**
 * Created by hannes on 09/05/14.
 */
public class ExternalCommandHandler {

    private float nitrogenDioxide;  //NO2
    private float groundLevelOzone; // O3
    private float fineParticles; // PM2.5
    private float respirableSuspendedParticle ; // PM10
    private float sulfurDioxide; // S02
    private float carbonMonoxide; // CO


    public static void handleCommand(String command)
    {
        if (command == null || command.length()<1)
        {
            return;
        }
        //
        Log.i(MainActivity.LOG_TAG,"received: "+command);
        String[] values = command.split(",");
        if (values.length>=6)
        {
            Measurement mea = new Measurement();
            mea.setNitrogenDioxide(Float.parseFloat(values[0]));
            mea.setGroundLevelOzone(Float.parseFloat(values[1]));
            mea.setFineParticles(Float.parseFloat(values[2]));
            mea.setRespirableSuspendedParticle(Float.parseFloat(values[3]));
            mea.setSulfurDioxide(Float.parseFloat(values[4]));
            mea.setCarbonMonoxide(Float.parseFloat(values[5]));
            Location location = null;
            if(values.length>=8)
            {
                location = new Location("");
                location.setLatitude(Float.parseFloat(values[6]));//52.366667, 4.9
                location.setLongitude(Float.parseFloat(values[7]));
            }
            EventBus.getDefault().post(new ExternalCommand(mea,location));
        }

    }
}
