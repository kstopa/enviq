package eu.esa.spaceappcamp.enviq.map;

import android.graphics.Bitmap;

import com.google.android.gms.maps.model.LatLngBounds;

import eu.esa.spaceappcamp.enviq.data.DataManager;
import eu.esa.spaceappcamp.enviq.data.Pollutant;
import eu.esa.spaceappcamp.enviq.models.MeasurementParameter;

public class OverlayImageRetrieval {

    private static Bitmap getPm10(LatLngBounds bufferedBounds) {
        return DataManager.getInstance().getPM10(bufferedBounds.southwest.latitude,
                bufferedBounds.southwest.longitude,
                bufferedBounds.northeast.latitude,
                bufferedBounds.northeast.longitude)
                .toBitmap();
    }

    private static Bitmap getPm25(LatLngBounds bufferedBounds) {
        return DataManager.getInstance().getPM25(bufferedBounds.southwest.latitude,
                bufferedBounds.southwest.longitude,
                bufferedBounds.northeast.latitude,
                bufferedBounds.northeast.longitude)
                .toBitmap();
    }

    private static Bitmap getCO(LatLngBounds bufferedBounds) {
        return DataManager.getInstance().getCarbonMonoxide(bufferedBounds.southwest.latitude,
                bufferedBounds.southwest.longitude,
                bufferedBounds.northeast.latitude,
                bufferedBounds.northeast.longitude)
                .toBitmap();
    }

    private static Bitmap getSO2(LatLngBounds bufferedBounds) {
        return DataManager.getInstance().getSulphurDioxide(bufferedBounds.southwest.latitude,
                bufferedBounds.southwest.longitude,
                bufferedBounds.northeast.latitude,
                bufferedBounds.northeast.longitude)
                .toBitmap();
    }

    private static Bitmap getO3(LatLngBounds bufferedBounds) {
        return DataManager.getInstance().getOzone(bufferedBounds.southwest.latitude,
                bufferedBounds.southwest.longitude,
                bufferedBounds.northeast.latitude,
                bufferedBounds.northeast.longitude)
                .toBitmap();
    }

    private static Bitmap getNO2(LatLngBounds bufferedBounds) {
        return DataManager.getInstance().getNitrogenDioxide(bufferedBounds.southwest.latitude,
                bufferedBounds.southwest.longitude,
                bufferedBounds.northeast.latitude,
                bufferedBounds.northeast.longitude)
                .toBitmap();
    }

    public static Bitmap get(MeasurementParameter parameter, LatLngBounds bounds) {
        LatLngBounds bufferedBounds = BoundsFactory.createBufferArea(1.0, 1.0, bounds.getCenter());
        Pollutant type = parameter.getType();
        if (type == Pollutant.PM10) {
            return getPm10(bufferedBounds);
        } else if (type == Pollutant.PM2P5) {
            return getPm25(bufferedBounds);
        } else if (type == Pollutant.CO) {
            return getCO(bufferedBounds);
        }  else if (type == Pollutant.NO2) {
            return getNO2(bufferedBounds);
        }  else if (type == Pollutant.O3) {
            return getO3(bufferedBounds);
        }  else if (type == Pollutant.SO2) {
            return getSO2(bufferedBounds);
        }

        // default, should not occur
        return getPm10(bounds);
    }
}