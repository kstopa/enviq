package eu.esa.spaceappcamp.enviq.detail;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import eu.esa.spaceappcamp.enviq.R;
import eu.esa.spaceappcamp.enviq.models.MeasurementParameter;

public class NumbersFragment extends Fragment {
    @InjectView(R.id.detailsLayout) ViewGroup description;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.numbers_fragment, container, false);
        ButterKnife.inject(this, view);
        Intent intent = getActivity().getIntent();
        fillUiWithData(extractParameters(intent));
        return view;
    }

    private List<MeasurementParameter> extractParameters(Intent intent) {
        return (ArrayList<MeasurementParameter>) intent.getSerializableExtra(DetailActivity.KEY_PARAMETERS);
    }

    private void fillUiWithData(List<MeasurementParameter> parameters) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        for(MeasurementParameter parameter : parameters) {
            description.addView(createParameterView(inflater, parameter));
        }
    }

    private View createParameterView(LayoutInflater inflater, MeasurementParameter parameter) {
        View view = inflater.inflate(R.layout.measurement_detail_item, null);
        buildHeader(parameter, view);
        buildTexts(parameter, view);
        return view;
    }

    private void buildTexts(MeasurementParameter parameter, View view) {
        TextView text = (TextView) view.findViewById(R.id.descriptionText);
//        text.setTextColor(getResources().getColor(parameter.getColor()));
        text.setText(parameter.getEducationalText());
    }

    private void buildHeader(MeasurementParameter parameter, View view) {
        TextView header = (TextView) view.findViewById(R.id.descriptionHeader);
        TextView limit = (TextView) view.findViewById(R.id.descriptionLimit);
        header.setTextColor(getResources().getColor(parameter.getColor()));
        header.setText(buildTitle(parameter));
        limit.setText(buildLimit(parameter));
    }

    private String buildLimit(MeasurementParameter parameter) {
        int maxValue = (int) parameter.getMaxValue();
        if (maxValue > 0) {
            return " " + getString(R.string.limitValue) + ": " + maxValue + " \u00B5g/m\u00B3";
        }
        return "";
    }

    private String buildTitle(MeasurementParameter parameter) {
        return parameter.getTitle() + " : " + (int) parameter.getValue() + " \u00B5g/m\u00B3";
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ButterKnife.reset(this);
    }
}
