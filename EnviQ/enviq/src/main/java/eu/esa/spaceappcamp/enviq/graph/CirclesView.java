/*
 * 	   Created by Daniel Nadeau
 * 	   daniel.nadeau01@gmail.com
 * 	   danielnadeau.blogspot.com
 * 
 * 	   Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
 */

package eu.esa.spaceappcamp.enviq.graph;

import java.util.ArrayList;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import eu.esa.spaceappcamp.enviq.MainActivity;
import eu.esa.spaceappcamp.enviq.models.MeasurementParameter;

public class CirclesView extends View implements ValueAnimator.AnimatorUpdateListener {

    public static final String KEY_CIRCLES = "Circles";
    public static final String KEY_SUPERBUNDLE = "Super";
    private ArrayList<MeasurementParameter> circles = new ArrayList<MeasurementParameter>();
    private int thickness = 5;
    private int aplphaBaseCircle = 80;
    private int alphaFilledCircle = 200;
    private Path path = new Path();
    private Paint paint = new Paint();
    private static final int circleStepDP = 10;
    private final int circleStepPX;


    public CirclesView(Context context) {
        super(context);
        circleStepPX = getCircleStepPX(context,circleStepDP);
    }

    public CirclesView(Context context, AttributeSet attrs) {
        super(context, attrs);
        circleStepPX = getCircleStepPX(context,circleStepDP);
    }

    private int getCircleStepPX(Context co, int stepDP)
    {
        final float scale = co.getResources().getDisplayMetrics().density;
        Log.i(MainActivity.LOG_TAG, "circleStep = : "+ (int) (stepDP * scale + 0.5f));
        return  (int) (stepDP * scale + 0.5f);

    }

    public void onDraw(Canvas canvas) {
        canvas.drawColor(Color.TRANSPARENT);
        paint.reset();
        paint.setAntiAlias(true);
        paint.setStrokeCap(Paint.Cap.ROUND);
        float midX, midY, radius;


        midX = getWidth() / 2;
        midY = getHeight() / 2;
        if (midX < midY) {
            radius = midX - 20;
        } else {
            radius = midY - 20;
        }

        float midXL = midX - 10;
        float midXR = midX + 10;

        float radiusL = radius;
        float radiusR = radius;

        for (MeasurementParameter circle : circles) {

            if (circle.isDrawLeft()) {
                //draw base Path
                path.reset();
                paint.setColor(getContext().getResources().getColor(circle.getColor()));
                paint.setStyle(Paint.Style.STROKE);
                paint.setAlpha(aplphaBaseCircle);
                paint.setStrokeWidth(thickness);
                path.addArc(new RectF(midXL - radiusL, midY - radiusL, midXL + radiusL, midY + radiusL),
                        90, 180);
                canvas.drawPath(path, paint);

                //2nd part
                paint.setAlpha(alphaFilledCircle);
                paint.setStrokeWidth(thickness + 2);
                float sweepAngle = floatCalculateCurrentValueAngle(circle.getMaxValue(), circle.getValue(),
                        circle.getAnimationPercent());
                if (sweepAngle > 180) {
                    sweepAngle = 180;
                }
                path.reset();
                path.addArc(new RectF(midXL - radiusL, midY - radiusL, midXL + radiusL, midY + radiusL),
                        90, sweepAngle);
                canvas.drawPath(path, paint);
                paint.setTextSize(Math.min(20,circleStepPX));
                paint.setStrokeWidth(2);
                float textLength = paint.measureText(circle.getTitle());
                canvas.drawText(circle.getTitle(), midXL - textLength, midY - radiusL - 1 - (thickness / 2), paint);

                radiusL -= circleStepPX;
            } else {
                path.reset();
                paint.setColor(getContext().getResources().getColor(circle.getColor()));
                paint.setStyle(Paint.Style.STROKE);
                paint.setAlpha(aplphaBaseCircle);
                paint.setStrokeWidth(thickness);
                path.addArc(new RectF(midXR - radiusR, midY - radiusR, midXR + radiusR, midY + radiusR),
                        270, 180);
                canvas.drawPath(path, paint);

                //2nd part
                paint.setAlpha(alphaFilledCircle);
                paint.setStrokeWidth(thickness + 2);
                float sweepAngle = floatCalculateCurrentValueAngle(circle.getMaxValue(), circle.getValue(),
                        circle.getAnimationPercent());
                if (sweepAngle > 180) {
                    sweepAngle = 180;
                }
                path.reset();
                path.addArc(new RectF(midXR - radiusR, midY - radiusR, midXR + radiusR, midY + radiusR),
                        360 + 90 - sweepAngle, sweepAngle);
                canvas.drawPath(path, paint);
                paint.setTextSize(Math.min(20,circleStepPX));
                paint.setStrokeWidth(1);
                canvas.drawText(circle.getTitle(), midXR, midY - radiusR - 1 - (thickness / 2), paint);
                radiusR -= circleStepPX;
            }
        }


    }

    private float floatCalculateCurrentValueAngle(float maxValue, float currnetValue, int percent) {
        if (currnetValue <0 || maxValue <0 )
        {
            return 0;
        }
        float sweepAngle = (180 / maxValue) * currnetValue;
        float animatedValue1Percent = sweepAngle / 100;
        return animatedValue1Percent * percent;
    }


    public void addSlice(MeasurementParameter slice, boolean startAnimation) {
        this.circles.add(slice);
        postInvalidate();
        if (!startAnimation) {
            postInvalidate();
            return;
        }
        slice.setAnimationPercent(0);
        MeasurementParameterAnimator animator = new MeasurementParameterAnimator((slice));
        animator.addUpdateListener(this);
        animator.start();
        postInvalidate();

    }


    public int getThickness() {
        return thickness;
    }

    public void setThickness(int thickness) {
        this.thickness = thickness;
        postInvalidate();
    }

    public void removeSlices() {
        for (int i = circles.size() - 1; i >= 0; i--) {
            circles.remove(i);
        }
        postInvalidate();
    }

    public ArrayList<MeasurementParameter> getParameters()
    {
        return this.circles;
    }


    @Override
    public void onAnimationUpdate(ValueAnimator animation) {
        MeasurementParameterAnimator anim = (MeasurementParameterAnimator) animation;
        anim.getParameter().setAnimationPercent((Integer) anim.getAnimatedValue());
        postInvalidate();
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        if (state == null) {
            return;
        }
        Bundle bundle = (Bundle) state;
        Parcelable parcel = bundle.getParcelable(KEY_SUPERBUNDLE);
        super.onRestoreInstanceState(parcel);
        ArrayList<MeasurementParameter> bundleCircles = (ArrayList<MeasurementParameter>) bundle.getSerializable
                (KEY_CIRCLES);
        if (bundleCircles == null)
        {
            return;
        }
        for (MeasurementParameter circle : bundleCircles) {
            circle.setAnimationPercent(100);//if we turn during animations
            circles.add(circle);
        }

    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable supPar = super.onSaveInstanceState();
        Bundle bundle = new Bundle();
        if (supPar != null) {
            bundle.putParcelable(KEY_SUPERBUNDLE, supPar);
        }
        bundle.putSerializable(KEY_CIRCLES, circles);
        return bundle;
    }
}
