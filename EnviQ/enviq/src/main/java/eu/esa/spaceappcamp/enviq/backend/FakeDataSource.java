package eu.esa.spaceappcamp.enviq.backend;

import eu.esa.spaceappcamp.enviq.data.DataSource;
import eu.esa.spaceappcamp.enviq.models.Measurement;

class FakeDataSource implements DataSource {

    private final Measurement measurement;

    public FakeDataSource(Measurement measurement) {
        this.measurement = measurement;
    }

    @Override
    public double getWeightedCarbonMonoxide(double latitude, double longitude) {
        return measurement.getCarbonMonoxide();
    }

    @Override
    public double getWeightedPM10(double latitude, double longitude) {
        return measurement.getRespirableSuspendedParticle();
    }

    @Override
    public double getWeightedSulphurDioxide(double latitude, double longitude) {
        return measurement.getSulfurDioxide();
    }

    @Override
    public double getWeightedOzone(double latitude, double longitude) {
        return measurement.getGroundLevelOzone();
    }

    @Override
    public double getWeightedNitrogenDioxide(double latitude, double longitude) {
        return measurement.getNitrogenDioxide();
    }

    @Override
    public double getWeightedPM25(double latitude, double longitude) {
        return measurement.getFineParticles();
    }
}
