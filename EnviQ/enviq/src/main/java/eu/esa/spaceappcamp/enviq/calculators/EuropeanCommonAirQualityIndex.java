package eu.esa.spaceappcamp.enviq.calculators;

import android.content.Context;

import eu.esa.spaceappcamp.enviq.R;
import eu.esa.spaceappcamp.enviq.models.Measurement;

import static eu.esa.spaceappcamp.enviq.utils.Numbers.max;

/**
 * http://www.obsairve.eu/index.php?option=com_content&view=article&id=7&Itemid=129&lang=en
 */
class EuropeanCommonAirQualityIndex extends IndexCalculator {

    public static final int MAX_FINE_PARTICLES = 60;
    public static final int MAX_SULPHUR_DIOXIDE = 500;
    public static final int MAX_BIG_PARTICLES = 100;
    public static final int MAX_CARBON_MONOXIDE = 20000;
    public static final int MAX_OZONE = 240;
    public static final int MAX_NITROGEN_DIOXIDE = 400;
    private final Context context;

    EuropeanCommonAirQualityIndex(Context context) {
        super(context.getString(R.string.indexCaqi));
        this.context = context;
    }

    @Override
    public int calculate(Measurement measurement) {
        int value1 = getNitronDioxideIndex(measurement.getNitrogenDioxide());
        int value2 = getOzonIndex(measurement.getGroundLevelOzone());
        int value3 = getParticleIndex(measurement.getRespirableSuspendedParticle());
        int value4 = getCarbonMonoxideIndex(measurement.getCarbonMonoxide());
        int value5 = getSulphurDioxide(measurement.getSulfurDioxide());
        int value6 = getFineParticleIndex(measurement.getFineParticles());
        return max(value1, value2, value3, value4, value5, value6);
    }

    private int getFineParticleIndex(int fineParticles) {
        if (fineParticles <= 10) {
            return 1;
        } else if (fineParticles <= 20) {
            return 2;
        } else if (fineParticles <= 30) {
            return 3;
        } else if (fineParticles <= MAX_FINE_PARTICLES) {
            return 4;
        }
        return 5;
    }

    private int getSulphurDioxide(int sulfurDioxide) {
        if (sulfurDioxide <= 50) {
            return 1;
        } else if (sulfurDioxide <= 100) {
            return 2;
        } else if (sulfurDioxide <= 350) {
            return 3;
        } else if (sulfurDioxide <= MAX_SULPHUR_DIOXIDE) {
            return 4;
        }
        return 5;
    }

    private int getParticleIndex(int respirableSuspendedParticle) {
        if (respirableSuspendedParticle <= 15) {
            return 1;
        } else if (respirableSuspendedParticle <= 30) {
            return 2;
        } else if (respirableSuspendedParticle <= 50) {
            return 3;
        } else if (respirableSuspendedParticle <= MAX_BIG_PARTICLES) {
            return 4;
        }
        return 5;
    }

    private int getCarbonMonoxideIndex(int carbonMonoxide) {
        if (carbonMonoxide <= 5000) {
            return 1;
        } else if (carbonMonoxide <= 7500) {
            return 2;
        } else if (carbonMonoxide <= 10000) {
            return 3;
        } else if (carbonMonoxide <= MAX_CARBON_MONOXIDE) {
            return 4;
        }
        return 5;
    }

    private int getOzonIndex(int groundLevelOzone) {
        if (groundLevelOzone <= 60) {
            return 1;
        } else if (groundLevelOzone <= 120) {
            return 2;
        } else if (groundLevelOzone <= 180) {
            return 3;
        } else if (groundLevelOzone <= MAX_OZONE) {
            return 4;
        }
        return 5;
    }

    private int getNitronDioxideIndex(int nitrogenDioxide) {
        if (nitrogenDioxide <= 50) {
            return 1;
        } else if (nitrogenDioxide <= 100) {
            return 2;
        } else if (nitrogenDioxide <= 200) {
            return 3;
        } else if (nitrogenDioxide <= MAX_NITROGEN_DIOXIDE) {
            return 4;
        }
        return 5;
    }

    @Override
    public String getAsString(int index) {
        switch(index) {
            case 1:
                return context.getString(R.string.veryLow);
            case 2:
                return context.getString(R.string.low);
            case 3:
                return context.getString(R.string.medium);
            case 4:
                return context.getString(R.string.high);
            default:
            case 5:
                return context.getString(R.string.veryhigh);
        }
    }

    @Override
    public String getAsLongString(int index) {
        return getAsString(index);
    }

    @Override
    public boolean needsNitrogenDioxide() {
        return true;
    }

    @Override
    public boolean needsGroundLevelOzone() {
        return true;
    }

    @Override
    public boolean needsFineParticles() {
        return true;
    }

    @Override
    public boolean needsRespirableSuspendedParticle() {
        return true;
    }

    @Override
    public boolean needsSulfurDioxide() {
        return true;
    }

    @Override
    public boolean needsCarbonMonoxide() {
        return true;
    }

    @Override
    public float getMaxForNitrogenDioxide() {
        return MAX_NITROGEN_DIOXIDE;
    }

    @Override
    public float getMaxForGroundLevelOzone() {
        return MAX_OZONE;
    }

    @Override
    public float getMaxForFineParticles() {
        return MAX_FINE_PARTICLES;
    }

    @Override
    public float getMaxForRespirableSuspendedParticle() {
        return MAX_BIG_PARTICLES;
    }

    @Override
    public float getMaxForSulfurDioxide() {
        return MAX_SULPHUR_DIOXIDE;
    }

    @Override
    public float getMaxForCarbonMonoxide() {
        return MAX_CARBON_MONOXIDE;
    }

    @Override
    public int getMaxIndex() {
        return 5;
    }
}
