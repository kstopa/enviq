/*
 * 	   Created by Daniel Nadeau
 * 	   daniel.nadeau01@gmail.com
 * 	   danielnadeau.blogspot.com
 * 
 * 	   Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
 */

package eu.esa.spaceappcamp.enviq.models;

import java.io.Serializable;

import android.animation.ValueAnimator;

import eu.esa.spaceappcamp.enviq.data.Pollutant;

public class MeasurementParameter implements ValueAnimator.AnimatorUpdateListener, Serializable {

    private final int color;
    private final int educationalText;
    private float value;
    private float maxValue;
    private final Pollutant type;
    private final boolean drawLeft;

    private int animationPercent = 0;

    public int getAnimationPercent() {
        return animationPercent;
    }

    public void setAnimationPercent(int animationPercent) {
        this.animationPercent = animationPercent;
    }


    protected MeasurementParameter(Pollutant pollutant, int color, int educationalText, boolean drawLeft) {
        this.type = pollutant;
        this.educationalText = educationalText;
        this.drawLeft = drawLeft;
        this.color = color;
    }

    public boolean isDrawLeft() {
        return drawLeft;
    }

    public float getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(float maxValue) {
        this.maxValue = maxValue;
    }

    public String getTitle() {
        return type.toString();
    }

    public int getColor() {
        return color;
    }


    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }


    @Override
    public void onAnimationUpdate(ValueAnimator animation) {
        animationPercent = (Integer)animation.getAnimatedValue();
    }

    public int getEducationalText() {
        return educationalText;
    }

    public Pollutant getType() {
        return type;
    }
}
