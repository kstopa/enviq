package eu.esa.spaceappcamp.enviq.data;

public class NoDataException extends Exception {

    public NoDataException() {
        super("no data");
    }
}
