package eu.esa.spaceappcamp.enviq.backend;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

import eu.esa.spaceappcamp.enviq.externalcommands.ExternalCommand;
import eu.esa.spaceappcamp.enviq.location.LocationUtil;

public class HandleExternalDataService extends IntentService {

    private static final String LOG_TAG = "EnviQ";
    private static final String COMMAND = "COMMAND";

    public static void handle(Context applicationContext, ExternalCommand command) {
        applicationContext.startService(create(applicationContext, command));
    }

    public static Intent create(Context context, ExternalCommand command) {
        Intent intent = new Intent(context, HandleExternalDataService.class);
        intent.putExtra(COMMAND, command);
        return intent;
    }

    public HandleExternalDataService() {
        super(HandleExternalDataService.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        final ExternalCommand command = intent.getParcelableExtra(COMMAND);
        Location location = command.getLocation();
        if (location != null) {
            ReverseLocationService.geoCodeLocation(getApplicationContext(),location);
            UpdateDataService.updateData(getApplicationContext(), command.getMeasurement(),location);
        } else {
            LocationUtil.querySingleLocation(getApplicationContext(), new LocationListener() {

                private Context co = getApplicationContext();
                @Override
                public void onLocationChanged(Location location) {
                    ReverseLocationService.geoCodeLocation(getApplicationContext(),location);
                    UpdateDataService.updateData(co,command.getMeasurement(), location);
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {}

                @Override
                public void onProviderEnabled(String provider) {}

                @Override
                public void onProviderDisabled(String provider) {}

            });
        }
    }
}
