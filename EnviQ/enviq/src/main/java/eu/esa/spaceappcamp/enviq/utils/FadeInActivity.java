package eu.esa.spaceappcamp.enviq.utils;

import android.animation.Animator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import eu.esa.spaceappcamp.enviq.R;

public abstract class FadeInActivity extends Activity {

    private View contentView;
    private int animationSpeed;

    protected static void startSilent(Activity context, Intent intent) {
        context.startActivity(intent);
        context.overridePendingTransition(0,0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        contentView = findViewById(android.R.id.content);
        cacheSpeed();
        appear();
    }

    private void cacheSpeed() {
        // Retrieve and cache the system's default animation time.
        animationSpeed = getResources().getInteger(android.R.integer.config_longAnimTime);
    }

    private void appear() {
        animate(0f, 0.8f, null);
    }

    private void animate(float from, float to, Animator.AnimatorListener listener) {

        // Set the content view to 0% opacity but visible, so that it is visible
        // (but fully transparent) during the animation.
        contentView.setAlpha(from);
        contentView.setVisibility(View.VISIBLE);

        // Animate the content view to 100% opacity, and clear any animation
        // listener set on the view.
        contentView.animate().alpha(to).setDuration(animationSpeed).setListener(listener);
    }
}
