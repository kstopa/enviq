package eu.esa.spaceappcamp.enviq.location;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;

import de.greenrobot.event.EventBus;

/**
 * Created by adamW on 08.05.14.
 */
public class GoogleLocationUtil implements

        ConnectionCallbacks,
        OnConnectionFailedListener,
        LocationListener {

    public static final String LOG_TAG = "EnviQ";

    private LocationClient GMSLocationClient;
    // Demo values
    private static final LocationRequest REQUEST = LocationRequest.create()
            .setInterval(5000)         // 5 seconds
            .setFastestInterval(16)    // 16ms = 60fps
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

    private void setUpLocationClientIfNeeded(Context context) {
        if (GMSLocationClient == null) {
            GMSLocationClient = new LocationClient(
                    context,
                    this,  // ConnectionCallbacks
                    this); // OnConnectionFailedListener
        }
    }

    public Location getLastKnownLocation(Context context) {
        if ( GMSLocationClient != null && GMSLocationClient.isConnected() ) {

            querySingleLocation(context);

            return GMSLocationClient.getLastLocation();
        }
        return getMockLocation();
    }

    public void queryLastKnownLocation(Context context) {

        setUpLocationClientIfNeeded(context);

        if ( GMSLocationClient != null && GMSLocationClient.isConnected() ) {
           EventBus.getDefault().post( GMSLocationClient.getLastLocation() );
        } else {
           EventBus.getDefault().post( getMockLocation() );
        }

    }
    // Same as Johannes.
    private static Location getMockLocation() {
        Location location;
        Log.e(LOG_TAG, "using mock location");
        location = new Location("");
        location.setLatitude(52.366667);//52.366667, 4.9
        location.setLongitude(4.9);
        return location;

    }

    public void querySingleLocation(Context context) {

        setUpLocationClientIfNeeded(context);
        if (GMSLocationClient != null && GMSLocationClient.isConnected()) {
           GMSLocationClient.requestLocationUpdates(REQUEST, this, context.getMainLooper());
        }

    }

    public void queryMultipleLocations(Context context, long minTimeMilliseconds, float minDistanceMeters,
                                              boolean pushLastKnown) {
        setUpLocationClientIfNeeded(context);

        if ( GMSLocationClient == null || !GMSLocationClient.isConnected() ) {
            EventBus.getDefault().post( getMockLocation() );
        }

        if (pushLastKnown) {
            EventBus.getDefault().post( GMSLocationClient.getLastLocation() );
        }

        LocationRequest Request = LocationRequest.create()
                .setInterval(minTimeMilliseconds)
                .setSmallestDisplacement(minDistanceMeters)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        GMSLocationClient.requestLocationUpdates(Request, this, context.getMainLooper());

    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onDisconnected() {

    }

    @Override
    public void onLocationChanged(Location location) {
        EventBus.getDefault().post(location);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
}
