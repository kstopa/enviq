package eu.esa.spaceappcamp.enviq.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Measurement implements Parcelable {

    // all values are in micrograms / cubic meter
    private float nitrogenDioxide;  //NO2
    private float groundLevelOzone; // O3
    private float fineParticles; // PM2.5
    private float respirableSuspendedParticle ; // PM10
    private float sulfurDioxide; // S02
    private float carbonMonoxide; // CO

    public int getNitrogenDioxide() {
        return (int)nitrogenDioxide;
    }

    public int getGroundLevelOzone() {
        return (int)groundLevelOzone;
    }

    public int getSulfurDioxide() {
        return (int)sulfurDioxide;
    }

    public void setSulfurDioxide(float sulfurDioxide) {
        this.sulfurDioxide = sulfurDioxide;
    }

    public int getCarbonMonoxide() {
        return (int)carbonMonoxide;
    }

    public void setCarbonMonoxide(float carbonMonoxide) {
        this.carbonMonoxide = carbonMonoxide;
    }

    public int getRespirableSuspendedParticle() {
        return (int)respirableSuspendedParticle;
    }

    public void setRespirableSuspendedParticle(float respirableSuspendedParticle) {
        this.respirableSuspendedParticle = respirableSuspendedParticle;
    }

    public int getFineParticles() {
        return (int)fineParticles;
    }

    public void setFineParticles(float fineParticles) {
        this.fineParticles = fineParticles;
    }

    public void setGroundLevelOzone(float groundLevelOzone) {
        this.groundLevelOzone = groundLevelOzone;
    }

    public void setNitrogenDioxide(float nitrogenDioxide) {
        this.nitrogenDioxide = nitrogenDioxide;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeFloat(this.nitrogenDioxide);
        dest.writeFloat(this.groundLevelOzone);
        dest.writeFloat(this.fineParticles);
        dest.writeFloat(this.respirableSuspendedParticle);
        dest.writeFloat(this.sulfurDioxide);
        dest.writeFloat(this.carbonMonoxide);
    }

    public Measurement() {
    }

    private Measurement(Parcel in) {
        this.nitrogenDioxide = in.readFloat();
        this.groundLevelOzone = in.readFloat();
        this.fineParticles = in.readFloat();
        this.respirableSuspendedParticle = in.readFloat();
        this.sulfurDioxide = in.readFloat();
        this.carbonMonoxide = in.readFloat();
    }

    public static Parcelable.Creator<Measurement> CREATOR = new Parcelable.Creator<Measurement>() {
        public Measurement createFromParcel(Parcel source) {
            return new Measurement(source);
        }

        public Measurement[] newArray(int size) {
            return new Measurement[size];
        }
    };
}
