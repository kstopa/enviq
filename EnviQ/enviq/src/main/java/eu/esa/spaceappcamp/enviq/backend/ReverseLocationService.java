package eu.esa.spaceappcamp.enviq.backend;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Location;
import android.util.Log;

import de.greenrobot.event.EventBus;

public class ReverseLocationService extends IntentService {

    private static final String LOG_TAG = "EnviQ";
    public static final String KEY_LOCATION = "Location";
    private ReverseLocator locator;
    private final EventBus eventBus = EventBus.getDefault();

    public static void geoCodeLocation(Context context, Location location) {
        Intent intent = new Intent(context, ReverseLocationService.class);
        intent.putExtra(KEY_LOCATION,location);
        context.startService(intent);
    }

    public ReverseLocationService() {
        super(ReverseLocationService.class.getName());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        locator = new ReverseLocator(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Location location = intent.getParcelableExtra(KEY_LOCATION);
        Log.i(LOG_TAG,"onHandleIntent - lat: "+location.getLatitude()+" long: "+location.getLongitude());
        notify(locator.reverse(location));
    }

    private void notify(Address object) {
        if (object != null) {
            eventBus.postSticky(object);
        }
    }
}
