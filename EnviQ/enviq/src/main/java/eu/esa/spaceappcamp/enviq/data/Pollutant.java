package eu.esa.spaceappcamp.enviq.data;

/**
 * Created by kstopa on 11.05.14.
 */
public enum Pollutant {

    CO("CO"),
    O3("O3", "O\u2083"),
    PM2P5("PM25"),
    PM10("PM10"),
    SO2("SO2", "SO\u2082"),
    NO2("NO2", "NO\u2082");

    private final String apiName;
    private final String name;

    private Pollutant(String pname) {
        this(pname, pname);
    }

    private Pollutant(String pname, String name) {
        this.apiName =pname;
        this.name = name;
    }

    public String toString() {
        return name;
    }

    public String getApiName() {
        return apiName;
    }
}
