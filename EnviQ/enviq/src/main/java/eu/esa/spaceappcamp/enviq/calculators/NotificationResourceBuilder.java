package eu.esa.spaceappcamp.enviq.calculators;

import android.content.Context;
import android.content.res.Resources;

import eu.esa.spaceappcamp.enviq.R;
import eu.esa.spaceappcamp.enviq.models.HealthIndex;

public class NotificationResourceBuilder {

    private final Resources resources;
    private final Context context;

    public NotificationResourceBuilder(Context context) {
        this.context = context;
        this.resources = context.getResources();
    }

    public int calculateLocalImageForCalculator(HealthIndex index) {
        int percentage = getAsPercentage(index);
        if (percentage < 34) {
            return R.drawable.quality_nice_flickr_8210393947;
        } else if (percentage < 67) {
            return R.drawable.quality_medium_flickr_11402055104;
        } else {
            return R.drawable.quality_bad_flickr_5685866403;
        }
    }

    private int getAsPercentage(HealthIndex index) {
        return index.getNumber()*100/IndexCalculator.getInstance(context).getMaxIndex();
    }

    public String calculateRemoteImageForCalculator(HealthIndex index) {

        int percentage = getAsPercentage(index);
        if (percentage < 41) {
            return "https://farm9.staticflickr.com/8341/8210393947_5b8f669ce8.jpg";
        } else if (percentage < 67) {
            return "https://farm6.staticflickr.com/5538/11402055104_59e44c9516.jpg";
        } else {
            return "https://farm6.staticflickr.com/5242/5685866403_70bc14aac1.jpg";
        }
    }

    public String getTitleString(HealthIndex healthIndex) {
        return "My air quality: " + healthIndex.getNumberToString() + " out of " +  IndexCalculator.getInstance(context).getMaxIndex();
    }

    public String getAppNameString() {
        return "enviQ"; // has to match facebook configuration
    }

    public int calculateLayoutForImage(HealthIndex index) {
        int percentage = getAsPercentage(index);
        if (percentage < 34) {
            return R.layout.notification_detail_good;
        } else if (percentage < 67) {
            return R.layout.notification_detail_medium;
        } else {
            return R.layout.notification_detail_bad;
        }
    }

    public int calculateColorForIndex(HealthIndex healthIndex) {
        int percentage = getAsPercentage(healthIndex);
        if (percentage < 15) {
            return asColor(android.R.color.holo_green_dark);
        } else if (percentage < 30) {
            return asColor(android.R.color.holo_green_light);
        } else if (percentage < 45) {
            return asColor(android.R.color.holo_orange_light);
        } else if (percentage < 70) {
            return asColor(android.R.color.holo_orange_dark);
        } else if (percentage < 85) {
            return asColor(android.R.color.holo_red_light);
        } else {
            return asColor(android.R.color.holo_red_dark);
        }
    }

    private int asColor(int id) {
        return resources.getColor(id);
    }

    public int getMaxValueForIndex(HealthIndex currentHealthIndex) {
        return IndexCalculator.getInstance(context).getMaxIndex();
    }
}
