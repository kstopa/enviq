package eu.esa.spaceappcamp.enviq.data.geometry;

/**
 * Created by kstopa on 08.05.14.
 */
public class Coordinate {

    double lat;
    double lon;

    public Coordinate() {
        lat = 0.0;
        lon = 0.0;
    }

    public Coordinate(double latitude, double longitude) {
        lat = latitude;
        lon = longitude;
    }

    public double getLatitude() {
        return lat;
    }

    public double getLongitude() {
        return lon;
    }

    public double getDistance(double latitude, double longitude) {
        double dlat = latitude - lat;
        double dlon = longitude - lon;
        return Math.sqrt(dlat*dlat+dlon*dlon);
    }
}
