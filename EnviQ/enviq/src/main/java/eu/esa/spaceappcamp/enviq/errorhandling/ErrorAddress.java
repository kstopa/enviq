package eu.esa.spaceappcamp.enviq.errorhandling;

import java.util.Locale;

import android.content.Context;
import android.location.Address;

import eu.esa.spaceappcamp.enviq.R;

/**
 * Created by hannes on 12/05/14.
 */
public class ErrorAddress {

    private ErrorAddress(){

    }

    public static Address create(Context context,double lat, double longit)
    {
        Address myAdd = new Address(Locale.getDefault());
        myAdd.setLongitude(longit);
        myAdd.setLatitude(lat);
        myAdd.setLocality(context.getString(R.string.nolocation));
        myAdd.setCountryName(context.getString(R.string.errorlocation));
        return myAdd;
    }
}
