package eu.esa.spaceappcamp.enviq.data;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by kstopa on 11.05.14.
 * @todo add capability to get historical data from server.
 * @todo Check locals and convert dates from local to CET.
 */
public class Forecast {

    /** Real date from which you want to get data.  */
    Calendar fdate;

    /**
     * Constructor that create forecast for today.
     */
    public Forecast() {
        fdate = Calendar.getInstance();
    }

    /**
     * Constructor that create forecast from h hours from now.
     * @param h Number of hours to forecast forward from now.
     */
    public Forecast(int h) {
        fdate = Calendar.getInstance();
        fdate.add(Calendar.HOUR_OF_DAY, h);
    }


    /**
     * Base on the forecast date and time return forecast code to be download
     * from the server based on reference data. (usually yesterday date)
     * fc0 -> yesterday
     * fc1 -> today
     * fc2 -> tomorrow
     * fc3 -> after tomorrow.
     * @return Forecast code string to be download.
     */
    public String toString() {
        int nday = fdate.get(Calendar.DATE) - Calendar.getInstance().get(Calendar.DATE);
        switch (nday) {
            case -1 :
                return "fc0";
            case 0 :
                return "fc1";
            case 1 :
                return "fc2";
            default:
                return "fc3";
        }
    }

    /**
     * Base on the forecast date and time return forecast code to be download
     * from the server based on reference data. (usually yesterday date)
     * fc0 -> yesterday
     * fc1 -> today
     * fc2 -> tomorrow
     * fc3 -> after tomorrow.
     * @return Forecast code string to be download.
     */
    public String getForecast() {
        return this.toString();
    }

    /**
     * Get date for http request to MACC WCS server.
     * @return Date to be parsed as a part of http request.
     */
    public Date getDate() {
        Calendar c = GregorianCalendar.getInstance();
        c.add(Calendar.DATE, -1);
        // Gets closest hours for one of availables at forecast (2,5,8,11,13,17,20,23)
        int h = ((2+(c.get(Calendar.HOUR_OF_DAY)/3))*3)-1;
        c.set(Calendar.HOUR_OF_DAY, h);
        return c.getTime();
    }
}
