package eu.esa.spaceappcamp.enviq.externalcommands;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by hannes on 09/05/14.
 */
class SocketListenerThread extends Thread
{

    private ServerSocket incomingSocket = null;

    SocketListenerThread(ServerSocket socket)
    {
        incomingSocket = socket;
    }
    @Override
    public void run(){
        while (incomingSocket != null&&!incomingSocket.isClosed()) {
            try {
                Socket clientConnection = incomingSocket.accept();
                OutputStream out = clientConnection.getOutputStream();
                out.write(("NO2,O3,PM2.5,PM10,SO2,CO,lat,lang"+'\n').getBytes());
                out.flush();
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(clientConnection.getInputStream()));
                String inputLine;
                while ((inputLine = in.readLine()) != null) {
                    ExternalCommandHandler.handleCommand(inputLine);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
