package eu.esa.spaceappcamp.enviq.models;

import android.content.Context;

import eu.esa.spaceappcamp.enviq.R;
import eu.esa.spaceappcamp.enviq.data.Pollutant;

public class MeasurementParameterFactory {

    private static final int nitrogenDioxideColor = android.R.color.holo_blue_bright;  //NO2
    private static final int groundLevelOzoneColor = android.R.color.holo_green_light; // O3
    private static final int fineParticlesColor = android.R.color.holo_blue_dark; // PM2.5
    private static final int respirableSuspendedParticleColor = android.R.color.holo_orange_light; // PM10
    private static final int sulfurDioxideColor = android.R.color.holo_purple; // S02
    private static final int carbonMonoxideColor = android.R.color.holo_green_dark; // CO

    private static final boolean nitrogenDioxideLeft = true;  //NO2
    private static final boolean groundLevelOzoneLeft = true; // O3
    private static final boolean fineParticlesLeft = true; // PM2.5
    private static final boolean respirableSuspendedParticleLeft = false; // PM10
    private static final boolean sulfurDioxideLeft = false; // S02
    private static final boolean carbonMonoxideLeft = false; // CO


    public static MeasurementParameter createNitrogenDioxideParameter(float currentValue, float maxValue) {
        MeasurementParameter parameter = new MeasurementParameter(Pollutant.NO2, nitrogenDioxideColor, R.string.description_no2, nitrogenDioxideLeft);
        parameter.setValue(currentValue);
        parameter.setMaxValue(maxValue);
        return parameter;
    }

    public static MeasurementParameter createGroundLevelOzoneParameter(float currentValue, float maxValue) {
        MeasurementParameter parameter = new MeasurementParameter(Pollutant.O3, groundLevelOzoneColor, R.string.description_o3, groundLevelOzoneLeft);
        parameter.setValue(currentValue);
        parameter.setMaxValue(maxValue);
        return parameter;
    }

    public static MeasurementParameter createFineParticlesParameter(float currentValue, float maxValue) {
        MeasurementParameter parameter = new MeasurementParameter(Pollutant.PM2P5, fineParticlesColor, R.string.description_pm25, fineParticlesLeft);
        parameter.setValue(currentValue);
        parameter.setMaxValue(maxValue);
        return parameter;
    }

    public static MeasurementParameter createRespirableSuspendedParticleParameter(float currentValue,
                                                                                  float maxValue) {
        MeasurementParameter parameter = new MeasurementParameter(Pollutant.PM10, respirableSuspendedParticleColor, R.string.description_pm,
                respirableSuspendedParticleLeft);
        parameter.setValue(currentValue);
        parameter.setMaxValue(maxValue);
        return parameter;
    }

    public static MeasurementParameter createSulfurDioxideParameter(float currentValue, float maxValue) {
        MeasurementParameter parameter = new MeasurementParameter(Pollutant.SO2, sulfurDioxideColor, R.string.description_so2, sulfurDioxideLeft);
        parameter.setValue(currentValue);
        parameter.setMaxValue(maxValue);
        return parameter;
    }

    public static MeasurementParameter createCarbonMonoxideParameter(float currentValue, float maxValue) {
        MeasurementParameter parameter = new MeasurementParameter(Pollutant.CO, carbonMonoxideColor, R.string.description_co, carbonMonoxideLeft);
        parameter.setValue(currentValue);
        parameter.setMaxValue(maxValue);
        return parameter;
    }
}
