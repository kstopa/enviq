package eu.esa.spaceappcamp.enviq.utils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;

import eu.esa.spaceappcamp.enviq.R;

public abstract class FacebookActivity extends Activity {

    public FacebookActivity() {
        super();
    }

    protected UiLifecycleHelper uiHelper;

    private Session.StatusCallback statusCallback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            Log.i("", "session status changed");
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        uiHelper = new UiLifecycleHelper(this, statusCallback);
        uiHelper.onCreate(savedInstanceState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        uiHelper.onActivityResult(requestCode, resultCode, data, new FacebookDialog.Callback() {
            @Override
            public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
                Log.e("", String.format("Error: %s", error.toString()));
            }

            @Override
            public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
                Log.i("", "Success!");
            }
        });
    }

    @Override
    public Intent getIntent() {
        return super.getIntent();
    }

    @Override
    public void onResume() {
        super.onResume();
        uiHelper.onResume();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);
    }

    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
    }

    @Override
    public void setIntent(Intent newIntent) {
        super.setIntent(newIntent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }

    protected void shareToFacebook(String name, String link, String picture, String description, String applicationName, String place) {
        try {
            FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(this)
                    .setName(name)
                    .setLink(link)
                    .setPicture(picture)
                    .setDescription(description)
                    .setApplicationName(applicationName)
                    .setPlace(place)
                    .build();
            uiHelper.trackPendingDialogCall(shareDialog.present());
        } catch(Exception e) {
            Log.e("", "facebook share error", e);
            Toast.makeText(this, getString(R.string.no_facebook), Toast.LENGTH_SHORT).show();
        }
    }
}
