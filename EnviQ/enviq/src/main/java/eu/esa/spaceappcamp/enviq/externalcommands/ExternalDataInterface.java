package eu.esa.spaceappcamp.enviq.externalcommands;

import java.io.IOException;
import java.net.ServerSocket;

/**
 * Created by hannes on 09/05/14.
 */
public class ExternalDataInterface {

    private ServerSocket incomingSocket = null;

    public void connect()
    {
        if (true)
        {
            return;
        }
        try {
            if (incomingSocket != null)
            {
                incomingSocket.close();
            }
            incomingSocket = new ServerSocket(13342);
            new SocketListenerThread(incomingSocket).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void disConnect()
    {
        try {
            if (incomingSocket != null)
            {
                incomingSocket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
