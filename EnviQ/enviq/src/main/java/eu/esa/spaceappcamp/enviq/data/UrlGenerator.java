package eu.esa.spaceappcamp.enviq.data;

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Generates the URL of the World Data Center WCS service to get ascii grid data.
 */
public class UrlGenerator {

	/* http://wdc.dlr.de/cgi-bin/produkt_4d_w?COVERAGE=macc_pm2p5_conc_fc0&service=wcs&version=1.0.0&request=getcoverage
	&crs=epsg:4326
	&bbox=-25,30,45,70
	&RESX=0.1&RESY=0.1
	&format=application/x-asciigrid-32f
	&TIME=2014-05-05T23&elevation=0.0
	&OUTPUTFILENAME=macc_pm2p5_conc_fc0_2014-05-05T23_0.0
	*/

	private final static String WWW_BASE = "http://wdc.dlr.de/cgi-bin/produkt_4d_w?";
	
	/**
	 * Get URL for WCS service where is located specified image resource using
     * defualt CRS, ascii grid format and 0.1 lat an lon resolution.
	 * @param layer Layer Name at WMS service.
	 * @param minx Minimal X coordinate of the boudings of the image.
	 * @param miny Minimal Y coordinate of the boudings of the image.
	 * @param maxx Maximal X coordinate
	 * @param maxy Maximal Y coordiante
	 * @param width Obtained image width
	 * @param height Obtained image height
	 * @param dt Image date.
	 * @return String with URL to the image resource.
	 */
	@SuppressLint("SimpleDateFormat")
	public static String getWcsResource(String layer, double minlat, double minlon, double maxlat, double maxlon, Date dt) {
		SimpleDateFormat dft = new SimpleDateFormat("yyyy-MM-dd HH");
		String req = WWW_BASE +
               "COVERAGE=" + layer +
               "&service=wcs" +
               "&version=1.0.0" +
               "&request=getcoverage" +
			   "&crs=epsg:4326" +
               "&bbox=" + String.valueOf(minlon) + "," + String.valueOf(minlat) + "," + String.valueOf(maxlon) + "," + String.valueOf(maxlat) + //-25,30,45,70
               "&RESX=0.1&RESY=0.1" +
               "&format=application/x-asciigrid-32f" +
               "&TIME=" + dft.format(dt).replace(" ", "T") +
               "&elevation=0.0" +
               "&OUTPUTFILENAME=enviq";
        return req;
	}

    /**
     * For testing porpueses that gets a url of checked PM2.5 product.
     * @return String with URL to the image resource
     */
    public static String getWcsTestingResource() {
       return "http://wdc.dlr.de/cgi-bin/produkt_4d_w?COVERAGE=macc_pm2p5_conc_fc0&service=wcs&version=1.0.0&request=getcoverage" +
            "&crs=epsg:4326" +
            "&bbox=-25,30,45,70" +
            "&RESX=0.1&RESY=0.1" +
            "&format=application/x-asciigrid-32f" +
            "&TIME=2014-05-08T23&elevation=0.0" +
            "&OUTPUTFILENAME=test";
    }
}
