package eu.esa.spaceappcamp.enviq.map;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.Toast;

//import com.example.mapdemo.OverlayDownloadTask;
import com.google.android.gms.maps.model.LatLngBounds;

import eu.esa.spaceappcamp.enviq.data.AsciiGrid;
import eu.esa.spaceappcamp.enviq.data.DataManager;
import eu.esa.spaceappcamp.enviq.models.MeasurementParameter;

/**
 * Created by adamW on 08.05.14.
 */
public class OverlayManager {

    public Bitmap image;
    public Context context;

    public Bitmap getOverlay() {
        if (image == null) {
            //loadFromResource(R.drawable.wms);
            createDefaultOverlay();
        }
        return image;
    }

    public Bitmap loadFromDataManager(LatLngBounds bounds, MeasurementParameter parameter) {

        double latDiff = Math.abs(bounds.northeast.latitude - bounds.southwest.latitude);
        double lonDiff = Math.abs(bounds.northeast.longitude - bounds.southwest.longitude);

        Double bufferLat = (1.0 - latDiff)/2.0;
        Double bufferLon = (1.0 - lonDiff)/2.0;

        // LatLngBounds bufferedBounds = BoundsFactory.createBufferArea(bufferLat, bufferLon, bounds.getCenter());
        return OverlayImageRetrieval.get(parameter, bounds);
    }

    public Bitmap createDefaultOverlay() {
        image = BitmapSource.getDefaultBitmap();
        return image;
    }
}
