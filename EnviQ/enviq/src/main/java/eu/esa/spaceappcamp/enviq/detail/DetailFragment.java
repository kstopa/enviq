package eu.esa.spaceappcamp.enviq.detail;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import eu.esa.spaceappcamp.enviq.R;
import eu.esa.spaceappcamp.enviq.calculators.IndexCalculator;
import eu.esa.spaceappcamp.enviq.map.MapActivity;
import eu.esa.spaceappcamp.enviq.models.HealthIndex;
import eu.esa.spaceappcamp.enviq.models.MeasurementParameter;

public class DetailFragment extends Fragment {

    @InjectView(R.id.numberText)
    TextView numberText;
    @InjectView(R.id.statusText)
    TextView statusText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail, container, false);
        ButterKnife.inject(this, view);
        Intent intent = getActivity().getIntent();
        fillUiWithData(extractIndex(intent));
        return view;
    }

    @OnClick(R.id.wantToSeeMore)
    public void showMap() {
        MapActivity.start(getActivity(), (ArrayList<MeasurementParameter>)getActivity().getIntent().getSerializableExtra(DetailActivity.KEY_PARAMETERS));
    }

    private HealthIndex extractIndex(Intent intent) {
        return (HealthIndex) intent.getSerializableExtra(DetailActivity.KEY_HEALTHINDEX);
    }

    private void fillUiWithData(HealthIndex index) {
        IndexCalculator instance = IndexCalculator.getInstance(getActivity());
        numberText.setText(index.getNumberToString() + " " +getString(R.string.outOf)
                + " " + instance.getMaxIndex() + " " + getString(R.string.basedOn) + " " + instance.getName());
        statusText.setText(index.getLongText());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ButterKnife.reset(this);
    }
}
