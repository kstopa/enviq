package eu.esa.spaceappcamp.enviq;

import android.app.Application;
import android.location.Location;
import android.util.Log;

import de.greenrobot.event.EventBus;
import eu.esa.spaceappcamp.enviq.alarm.AlarmHandler;
import eu.esa.spaceappcamp.enviq.backend.HandleExternalDataService;
import eu.esa.spaceappcamp.enviq.backend.ReverseLocationService;
import eu.esa.spaceappcamp.enviq.backend.UpdateDataService;
import eu.esa.spaceappcamp.enviq.errorhandling.ErrorAddress;
import eu.esa.spaceappcamp.enviq.errorhandling.LocationError;
import eu.esa.spaceappcamp.enviq.externalcommands.ExternalCommand;
import eu.esa.spaceappcamp.enviq.location.LocationUtil;
import eu.esa.spaceappcamp.enviq.models.CompleteMeasurementData;
import eu.esa.spaceappcamp.enviq.models.HealthIndex;
import eu.esa.spaceappcamp.enviq.models.IndexChangeEvent;
import eu.esa.spaceappcamp.enviq.utils.EnviQNotification;


public class EnviQApplication extends Application {

    private final EventBus eventBus = EventBus.getDefault();
    private Location currentLocation = null;
    private boolean running = false;

    @Override
    public void onCreate() {
        super.onCreate();
        eventBus.register(this, 1);
        AlarmHandler.scheduleAlarm(this);
    }

    public synchronized void triggerLocation() {
        Log.i(MainActivity.LOG_TAG,"triggerLocation");
        if (running)
        {
            Log.i(MainActivity.LOG_TAG,"already running");
            return;
        }
        running = true;
        LocationUtil.querySingleLocation(getApplicationContext());
    }

    public synchronized boolean isRunning() {
        return this.running;
    }

    public void setLocation(Location location)
    {
        currentLocation = location;
    }

    /**
     * sometimes the app is in the backgroud while the computing is executed and does not restart on relaunch
     *
     * @param index
     */
    public void onEventBackgroundThread(HealthIndex index) {
        synchronized (this) {
            this.running = false;
        }
    }


    public void onEventMainThread(IndexChangeEvent event) {
        updateData(currentLocation);
    }

    public void onEventBackgroundThread(Location location) {
        this.currentLocation = location;
        updateData(location);
        ReverseLocationService.geoCodeLocation(getApplicationContext(), location);
    }

    public void onEventBackgroundThread(LocationError error) {
        if (BuildConfig.DEBUG) {
            this.currentLocation = LocationUtil.getMockLocation();
            updateData(currentLocation);
            ReverseLocationService.geoCodeLocation(getApplicationContext(), currentLocation);
        }
        else
        {
            //error healthindex
            this.currentLocation = null;
            EventBus.getDefault().post(ErrorAddress.create(this,0,0));
            EventBus.getDefault().post(HealthIndex.getErrorIndex(this));
        }
    }

    public void onEventBackgroundThread(ExternalCommand command) {
        HandleExternalDataService.handle(getApplicationContext(), command);
    }

    public void onEvent(CompleteMeasurementData data) {
        pushNotificationInBackground(data);
    }

    private void pushNotificationInBackground(final CompleteMeasurementData data) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                EnviQNotification.pushNotification(EnviQApplication.this, data.getParameters(), data.getIndex(),
                        data.getLocation());
            }
        }).start();
    }

    private void updateData(Location location) {
        UpdateDataService.updateData(getApplicationContext(), location);
    }
}
