package eu.esa.spaceappcamp.enviq.data;

import android.graphics.Bitmap;

import eu.esa.spaceappcamp.enviq.data.geometry.Coordinate;
import eu.esa.spaceappcamp.enviq.utils.ColorScale;

/**
 * Created by kstopa on 06.05.14.
 */
public class AsciiGrid {

    double[][] data;
    int cols;
    int rows;
    double xll;
    double yll;
    double csize;
    double nodata;

    double minval;
    double maxval;

    /**
     * Constructor that generates empty grid.
     */
    public AsciiGrid() {
        cols = 0;
        rows = 0;
        data = new double[1][1];
        yll = 0.0;
        xll = 0.0;
        nodata = -999;
        csize = 0.0;
    }

    /**
     * Constructor that initializes data for given size.
     * @param nr Total number of rows.
     * @param nc Total number of columns.
     */
    public AsciiGrid(int nr, int nc) {
        this();
        cols = nc;
        rows = nr;
        data = new double[rows][cols];
    }

    /**
     * Constructor based on data from Ascii Grid header content.
     * @param nr Total number of rows.
     * @param nc Total number of columns.
     * @param latmin (y)
     * @param lonmin (x)
     * @param cellSize Cell size (pixel size)
     * @param ndata No data value.
     */
    public AsciiGrid(int nr, int nc, double latmin, double lonmin, double cellSize, double ndata) {
        cols = nc;
        rows = nr;
        data = new double[rows][cols];
        csize = cellSize;
        xll = lonmin;
        yll = latmin;
        nodata = ndata;
    }


    public void setOrigin(double lat, double lon) {
        xll = lon;
        yll = lat;
    }

    public Coordinate getOrigin() {
        return new Coordinate(yll, xll);
    }

    public Coordinate getMinCoordinate() {
        return getOrigin();
    }

    public Coordinate getMaxCoordinate() {
        return new Coordinate(yll + (rows*csize), xll + (cols*csize));
    }


    public void setCellSize(double cellSize) {
        csize = cellSize;
    }

    public double getCellSize() {
        return csize;
    }

    /**
     * Sets value for NoData cells.
     * @param nd
     */
    public void setNoData(double nd) {
        nodata = nd;
    }

    /**
     *
     */
    public double getNoData() {
        return nodata;
    }

    /**
     * Get total number of rows.
     * @return Number of rows.
     */
    public int getRows() {
        return rows;
    }

    /**
     * Get total number of columns.
     * @return Total number of columns.
     */
    public int getCols() {
        return cols;
    }



    /**
     * Check if ascii grids contains data.
     * @return True if is empty.
     */
    public boolean isEmpty() {
        if ((cols <= 0) || (rows <= 0) || (csize <= 0.0)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Checks if a coordinate is located over the grid.
     * @param lat Latitude (y)
     * @param lon Longitude (x)
     * @return True if given coordinate is located at the grid.
     */
    public boolean contains(double lat, double lon) {
        if (isEmpty()) {
            return false;
        } else {
            if ((lat >= yll) && (lon >= xll) &&
                (lat <= yll+(csize*rows)) && (lon <= xll + (csize*cols))) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Set data at grid at given position.
     * @param r Row number.
     * @param c Column number.
     * @param val Data value.
     */
    public void setData(int r, int c, double val) {
        if ((r >= 0) && (c >= 0) &&
            (r < rows) && (c < cols)) {
            setMatrixData(r, c, val);
        } else {
            // @Todo expand and set?
        }
    }

    /**
     * Set data at given coordinate.
     * @param lat Latitude (y)
     * @param lon Longitude (x)
     * @param val Data value.
     */
    public void setData(double lat, double lon, double val) {
        // Get row and col for the given location
        if (contains(lat, lon)) {
            int c = (int)((lon - xll) / csize);
            int r = (int)((lat - yll) / csize);
            setMatrixData(r, c, val);
        } else {
            // @Todo expand and set?
        }
    }

    private void setMatrixData(int r, int c, double val) {
        data[r][c] = val;
        if (val > maxval) maxval = val;
        if (val < minval) minval = val;
    }


    public double getData(int r, int c) {
        if ((r >= 0) && (c >= 0) &&
            (r < rows) && (c < cols)) {
            return data[r][c];
        } else {
            return nodata;
        }
    }


    /**
     * Get data value for given location.
     * @param lat Latitude (y)
     * @param lon Longitude (x)
     * @return Value at given location.
     */
    public double getData(double lat, double lon) {
        if (contains(lat, lon)) {
            int r = (rows - 1) - (int)((lat - yll) / csize);  // (rows - 1) ?
            int c = (int)((lon - xll) / csize);
            return data[r][c];
        } else {
            return nodata;
        }
    }


    /**
     * Get data for an area given by it location bounding box.
     * @param minlat
     * @param minlon
     * @param maxlat
     * @param maxlon
     * @return
     */
    public AsciiGrid getData(double minlat, double minlon, double maxlat, double maxlon) {
        int minr = Math.max((rows-1) - (int)Math.floor((maxlat - yll) / csize), 0);
        int minc = Math.max((int)Math.floor((minlon - xll)/csize), 0);
        int maxr = Math.min( rows-1 - (int)Math.ceil((minlat - yll) / csize), rows-1 );
        int maxc = Math.min((int)Math.ceil((maxlon - xll) / csize) - 1, cols-1);
        return getData(minr, minc, maxr, maxc);
    }

    /**
     * Get data for given area.
     * @param minrow
     * @param mincol
     * @param maxrow
     * @param maxcol
     * @return AsciiGrid with a subset of data.
     */
    public AsciiGrid getData(int minrow, int mincol, int maxrow, int maxcol) {
        AsciiGrid clip = new AsciiGrid(maxrow - minrow + 1, maxcol - mincol + 1);
        clip.setCellSize(this.getCellSize());
        clip.setNoData(this.getNoData());
        double inilon = xll + minrow*csize;
        double inilat = yll + (rows-maxrow-1)*csize;
        clip.setOrigin(inilat, inilon);
        for (int nr=minrow; nr <= maxrow; nr++) {
            for (int nc=mincol; nc <= maxcol; nc++) {
                clip.setData(nr-minrow, nc-mincol, data[nr][nc]);
            }
        }
        return clip;
    }

    /*
     * Gets the value of the inverse distance weighting (IDW) for given location
     * based on grid's data.
     *
     */
    public double getWeightedData(double lat, double lon) {
        if (contains(lat, lon)) {
            //Log.d("ENVIQ", "Data original = " + getData(lat, lon));
            double dr2 = csize/2.0;
            int c = (int)Math.round((lon + dr2 - xll) / csize);
            int r = (rows-1) - (int)Math.round((lat + dr2 - yll) / csize);
            // Get closest cells
            int cmin = Math.max(c-1, 0);
            int rmin = Math.max(r-1, 0);
            int cmax = Math.min(c+1, cols-1);
            int rmax = Math.min(r+1, rows-1);
            //Get coordiantes of each cell
            Coordinate c0 = getCellCoordinate(rmin, cmin);
            Coordinate c1 = getCellCoordinate(rmin, cmax);
            Coordinate c2 = getCellCoordinate(rmax, cmax);
            Coordinate c3 = getCellCoordinate(rmax, cmax);
            // Get distances to each coordinate
            double d0 = c0.getDistance(lat, lon);
            double d1 = c1.getDistance(lat, lon);
            double d2 = c2.getDistance(lat, lon);
            double d3 = c3.getDistance(lat, lon);
            double dsum = (1.0/(d0*d0))+(1.0/(d1*d1))+(1.0/(d2*d2))+(1.0/(d3*d3));
            // Calc weight
            double w0 = (1.0/(d0*d0))/dsum;
            double w1 = (1.0/(d1*d1))/dsum;
            double w2 = (1.0/(d2*d2))/dsum;
            double w3 = (1.0/(d3*d3))/dsum;
            // Calc and return weighted value
            double val = data[rmin][rmax]*w0+data[rmin][cmax]*w1 +
                         data[rmax][rmax]*w2+data[rmax][cmax]*w3;
            return val;
        } else {
            return nodata;
        }
    }

    public double getMaxValue() {
        return maxval;
    }

    public double getMinValue() {
        return minval;
    }


    /**
     * Get coordinate for the central point at grids cell.
     * @param r
     * @param c
     * @return
     */
    private Coordinate getCellCoordinate(int r, int c) {
        Coordinate coor = new Coordinate();
        if ((r >= 0) && (c >= 0) &&
            (r < rows) && (c < cols)) {
            double lat = yll + ((rows-r)*csize + (csize/2.0));
            double lon = xll + (c*csize + (csize/2.0));
            coor = new Coordinate(lat, lon);
        }
        return coor;
    }


    public Bitmap toBitmap() {
        return toBitmap(maxval);
    }

    /**
     * Gets an RGB using color scale set to given maximal value.
     * @param max Maximal value for color scale.
     * @return RGB bitmap.
     */
    public Bitmap toBitmap(double max) {
        Bitmap map = Bitmap.createBitmap(cols, rows, Bitmap.Config.ARGB_8888);
        ColorScale scale = new ColorScale(max);  //getMaxValue());
        for (int nr=0; nr < rows; nr++) {
            for (int nc=0; nc< cols; nc++) {
                map.setPixel(nc, nr, scale.getColor(data[nr][nc]));
            }
        }
        return map;
    }

}
