package eu.esa.spaceappcamp.enviq.calculators;

import android.content.Context;

import eu.esa.spaceappcamp.enviq.R;
import eu.esa.spaceappcamp.enviq.models.Measurement;

/**
 * AQHI, http://en.wikipedia.org/wiki/Air_Quality_Health_Index
 */
class CanadianHealthIndex extends IndexCalculator {

    private final Context context;

    CanadianHealthIndex(Context context) {
        super(context.getString(R.string.indexCanada));
        this.context = context;
    }

    @Override
    public int calculate(Measurement measurement) {
        double summand1 = Math.expm1(0.000537f *measurement.getGroundLevelOzone());
        double summand2 = Math.expm1(0.000871f *measurement.getNitrogenDioxide());
        double summand3 = Math.expm1(0.000487f *measurement.getFineParticles());
        return getAsMaxTen(getAsNonZero((int) Math.round(summand1 + summand2 + summand3 * (1000f / 10.4f))));
    }

    private int getAsMaxTen(int nonZero) {
        return nonZero > 10 ? 10 : nonZero;
    }

    private int getAsNonZero(int round) {
        return round > 0 ? round : 1;
    }

    @Override
    public String getAsString(int index) {

        // TODO this is for risk population, add normal ones

        if (index <= 3) {
            return context.getString(R.string.canadian_verygood);
        } else if (index <= 6 ) {
            return context.getString(R.string.canadian_good);
        } else if (index <= 10) {
            return context.getString(R.string.canadian_not_good);
        } else {
            return context.getString(R.string.canadian_bad);
        }
    }

    @Override
    public String getAsLongString(int index) {
        return getAsString(index);
    }

    @Override
    public boolean needsNitrogenDioxide() {
        return true;
    }

    @Override
    public boolean needsGroundLevelOzone() {
        return true;
    }

    @Override
    public boolean needsFineParticles() {
        return true;
    }

    @Override
    public boolean needsRespirableSuspendedParticle() {
        return false;
    }

    @Override
    public boolean needsSulfurDioxide() {
        return false;
    }

    @Override
    public boolean needsCarbonMonoxide() {
        return false;
    }

    @Override
    public float getMaxForNitrogenDioxide() {
        return EuropeanCommonAirQualityIndex.MAX_NITROGEN_DIOXIDE;
    }

    @Override
    public float getMaxForGroundLevelOzone() {
        return EuropeanCommonAirQualityIndex.MAX_OZONE;
    }

    @Override
    public float getMaxForFineParticles() {
        return EuropeanCommonAirQualityIndex.MAX_FINE_PARTICLES;
    }

    @Override
    public float getMaxForRespirableSuspendedParticle() {
        return 0;// not supported
    }

    @Override
    public float getMaxForSulfurDioxide() {
        return 0;// not supported
    }

    @Override
    public float getMaxForCarbonMonoxide() {
        return 0; // not supported
    }

    @Override
    public int getMaxIndex() {
        return 10;
    }
}
