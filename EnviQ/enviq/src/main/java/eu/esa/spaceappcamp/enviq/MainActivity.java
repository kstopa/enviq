package eu.esa.spaceappcamp.enviq;


import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.transition.Fade;
import android.transition.TransitionManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import com.crashlytics.android.Crashlytics;
import de.greenrobot.event.EventBus;
import eu.esa.spaceappcamp.enviq.calculators.NotificationResourceBuilder;
import eu.esa.spaceappcamp.enviq.detail.DetailActivity;
import eu.esa.spaceappcamp.enviq.detail.ThatsUsFragment;
import eu.esa.spaceappcamp.enviq.externalcommands.ExternalDataInterface;
import eu.esa.spaceappcamp.enviq.graph.CirclesView;
import eu.esa.spaceappcamp.enviq.models.CompleteMeasurementData;
import eu.esa.spaceappcamp.enviq.models.HealthIndex;
import eu.esa.spaceappcamp.enviq.models.IndexChangeEvent;
import eu.esa.spaceappcamp.enviq.models.MeasurementParameter;
import eu.esa.spaceappcamp.enviq.settings.SettingsActivity;
import eu.esa.spaceappcamp.enviq.utils.BackgroundAnimation;
import eu.esa.spaceappcamp.enviq.utils.ClearEvent;
import eu.esa.spaceappcamp.enviq.utils.FacebookActivity;

public class MainActivity extends FacebookActivity {

    public static final String LOG_TAG = "EnviQApplication";
    public static final String KEY_ACTIVE_LOADING = "ActiveLoading";
    public static final String KEY_CITY = "City";
    public static final String KEY_LOCATION = "Location";
    public static final String KEY_LOCATIONSTRING = "LocationString";
    public static final String KEY_CALCULATED_VALUE = "CalculatedValue";
    public static final String KEY_CALCULATED_TEXT = "CalculatedText";
    public static final String KEY_CURRENT_ADDRESS = "'Key_Current_Address";
    public static final String KEY_CURRENT_HEALTHINDEX = "KEY_CURRENT_HEALTHINDEX";
    public static final String KEY_PARAMETERS = "Parameters";
    public static final String KEY_LATEST_INTENTID = "last_intent_id";
    public static final int REQUEST_CODE = 1001001;

    private final EventBus eventBus = EventBus.getDefault();
    private Address currentAddress = new Address(Locale.getDefault());
    private NotificationResourceBuilder builder;
    private Location currentLocation;
    private HealthIndex currentHealthIndex;
    ExternalDataInterface externalCommandInstance;
    @InjectView(R.id.rootLayout)
    View rootLayout;
    @InjectView(R.id.theNumber)
    TextView indexNumberView;
    @InjectView(R.id.theMaxNumber)
    TextView maxNumberView;
    @InjectView(R.id.indexText)
    TextView indexTextView;
    @InjectView(R.id.circles)
    CirclesView circlesView;
    @InjectView(R.id.location_city)
    TextView city;
    @InjectView(R.id.location_position)
    TextView geoLocation;
    @InjectView(R.id.progress)
    ProgressBar progressBar;

    @OnClick(R.id.theNumber)
    public void onNumberClick() {
        if (currentHealthIndex == null) {
            Toast.makeText(this, getString(R.string.patience), Toast.LENGTH_SHORT).show();
            return;
        }
        DetailActivity.start(MainActivity.this, circlesView.getParameters(), currentAddress, currentHealthIndex);
    }

    @Override
    protected void onStart() {
        super.onStart();
        eventBus.register(this, 9);
    }

    @Override
    protected void onStop() {
        eventBus.unregister(this);
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Crashlytics.start(this);

        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);
        builder = new NotificationResourceBuilder(this);

        if (savedInstanceState != null) {
            readFromBundle(savedInstanceState);
        } else if (intentValid(getIntent())) {
            readFromIntent(getIntent());
        } else {
            invalidateOptionsMenu();
            //startLocationSearch();
        }
        externalCommandInstance = new ExternalDataInterface();
        externalCommandInstance.connect();
    }

    private boolean intentValid(Intent intent) {
        if (!intent.hasExtra(KEY_CURRENT_HEALTHINDEX)) {
            return false;
        }
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if (prefs.getLong(KEY_LATEST_INTENTID, -1L) != intent.getLongExtra(KEY_LATEST_INTENTID, 0)) {
            return true;
        }
        return false;
    }

    private void invalidateIntent(Intent intent) {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefs.edit().putLong(KEY_LATEST_INTENTID, intent.getLongExtra(KEY_LATEST_INTENTID, -100)).commit();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        externalCommandInstance.disConnect();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intentValid(intent)) {
            readFromIntent(intent);
        }
    }

    private void readFromBundle(Bundle savedInstanceState) {
        currentHealthIndex = (HealthIndex) savedInstanceState.getSerializable(KEY_CURRENT_HEALTHINDEX);
        //noinspection ResourceType
        progressBar.setVisibility(savedInstanceState.getInt(KEY_ACTIVE_LOADING, progressBar.getVisibility()));
        city.setText(savedInstanceState.getString(KEY_CITY, city.getText().toString()));
        geoLocation.setText(savedInstanceState.getString(KEY_LOCATIONSTRING, geoLocation.getText().toString()));
        setTextToNumberView(savedInstanceState.getString(KEY_CALCULATED_VALUE, indexNumberView.getText().toString
                ()), 0);
        indexTextView.setText(savedInstanceState.getString(KEY_CALCULATED_TEXT, indexTextView.getText().toString
                ()));
        currentAddress = savedInstanceState.getParcelable(KEY_CURRENT_ADDRESS);
        currentLocation = savedInstanceState.getParcelable(KEY_LOCATION);
    }

    private void readFromIntent(Intent intent) {
        clearMeasurement();
        currentHealthIndex = (HealthIndex) intent.getSerializableExtra(KEY_CURRENT_HEALTHINDEX);
        progressBar.setVisibility(View.GONE);
        setTextToNumberView(currentHealthIndex.getNumberToString(), 0);
        setTextToIndexView(currentHealthIndex.getText());
        currentAddress = getIntent().getParcelableExtra(KEY_CURRENT_ADDRESS);
        city.setText(currentAddress.getLocality());
        geoLocation.setText(currentAddress.getCountryName());
        circlesView.removeSlices();
        List<MeasurementParameter> parameters = (ArrayList<MeasurementParameter>) intent.getSerializableExtra
                (KEY_PARAMETERS);
        for (MeasurementParameter parameter : parameters) {
            parameter.setAnimationPercent(100);
            circlesView.addSlice(parameter, false);
        }
        currentLocation = intent.getParcelableExtra(KEY_LOCATION);
        ((EnviQApplication) getApplicationContext()).setLocation(currentLocation);
        invalidateIntent(intent);

    }

    private void setTextToNumberView(String text, int animationTime) {
        if (text == null || text.length() == 0) {
            maxNumberView.setVisibility(View.INVISIBLE);
            indexNumberView.setVisibility(View.GONE);
        } else {
            indexNumberView.setTextColor(builder.calculateColorForIndex(currentHealthIndex));
            applyBackgroundForIndex(builder.calculateLocalImageForCalculator(currentHealthIndex), animationTime);
            addTransition((ViewGroup) indexNumberView.getParent());
            maxNumberView.setVisibility(View.VISIBLE);
            indexNumberView.setVisibility(View.VISIBLE);
            maxNumberView.setText("/" + builder.getMaxValueForIndex(currentHealthIndex));

        }
        indexNumberView.setText(text);

    }

    private void addTransition(ViewGroup parent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            TransitionManager.beginDelayedTransition(parent,
                    new Fade().setDuration(1500));
        }
    }

    @SuppressWarnings("deprecation")
    private void applyBackgroundForIndex(int newBackgroundID, int animationLength) {
        BackgroundAnimation.startAnimation(getApplicationContext(), rootLayout, newBackgroundID, animationLength);
    }

    @Override
    public void onResume() {
        super.onResume();
        EnviQApplication app = (EnviQApplication) getApplicationContext();
        if (this.currentHealthIndex == null && !app.isRunning()) {
            app.triggerLocation(); //no data, no search, start search
        }
    }

    private String getCityName() {
        return currentAddress.getLocality();
    }

    @SuppressWarnings("UnusedDeclaration")
    public void onEventBackgroundThread(Location location) {
        this.currentLocation = location;
    }

    @SuppressWarnings("UnusedDeclaration")
    public void onEventMainThread(IndexChangeEvent event) {
        circlesView.removeSlices();
        setTextToNumberView("", 0);
        setTextToIndexView("");
    }

    @SuppressWarnings("UnusedDeclaration")
    public void onEventMainThread(HealthIndex index) {
        this.currentHealthIndex = index;
        progressBar.setVisibility(View.GONE);
        setTextToNumberView(index.getNumberToString(), 2000);
        setTextToIndexView(index.getText());
        invalidateOptionsMenu();
    }

    @SuppressWarnings("UnusedDeclaration")
    public void onEventMainThread(MeasurementParameter slice) {
        circlesView.addSlice(slice, true);
    }

    @SuppressWarnings("UnusedDeclaration")
    public void onEventMainThread(ClearEvent clear) {
        clearMeasurement();
    }

    @SuppressWarnings("UnusedDeclaration")
    public void onEvent(CompleteMeasurementData data) {
        eventBus.cancelEventDelivery(data);
    }

    @SuppressWarnings("UnusedDeclaration")
    public void onEventMainThread(Address address) {
        currentAddress = address;
        city.setText(address.getLocality());
        geoLocation.setText(address.getCountryName());
        EventBus.getDefault().removeStickyEvent(address);
    }

    private void setTextToIndexView(String text) {
        if (text == null || text.length() == 0) {
            indexTextView.setText("");
        } else {
            indexTextView.setText(getString(R.string.airLevelText) + ": " + text);
        }
    }

    private void clearMeasurement() {
        circlesView.removeSlices();
        setTextToIndexView("");
        setTextToNumberView("", 0);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_facebook).setVisible(currentHealthIndex != null);
        menu.findItem(R.id.action_settings).setVisible(currentHealthIndex != null);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            startActivityForResult(new Intent(this, SettingsActivity.class), REQUEST_CODE);
            return true;
        } else if (id == R.id.action_facebook) {
            shareToFacebook();
        } else if (id == R.id.action_about) {
            showAbout();
        }
        return super.onOptionsItemSelected(item);
    }

    private void showAbout() {
        getFragmentManager().beginTransaction().add(android.R.id.content, new ThatsUsFragment()).addToBackStack
                ("").commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            EventBus.getDefault().post(new IndexChangeEvent());
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_ACTIVE_LOADING, progressBar.getVisibility());
        outState.putString(KEY_CITY, this.city.getText().toString());
        outState.putString(KEY_LOCATIONSTRING, this.geoLocation.getText().toString());
        outState.putString(KEY_CALCULATED_VALUE, indexNumberView.getText().toString());
        outState.putString(KEY_CALCULATED_TEXT, indexTextView.getText().toString());
        outState.putParcelable(KEY_CURRENT_ADDRESS, currentAddress);
        outState.putSerializable(KEY_CURRENT_HEALTHINDEX, currentHealthIndex);
        outState.putParcelable(KEY_LOCATION, currentLocation);
    }

    private void shareToFacebook() {
        shareToFacebook(currentHealthIndex);
    }

    private void shareToFacebook(HealthIndex healthIndex) {
        String name = builder.getTitleString(healthIndex);
        String link = "https://play.google.com/store/apps/details?id=eu.esa.spaceappcamp.enviq";

        // get picture for post
        String picture = builder.calculateRemoteImageForCalculator(healthIndex);

        String description = healthIndex.getText();
        if (!healthIndex.getLongText().equals(description)) {
            description += " " + healthIndex.getLongText();
        }

        String applicationName = builder.getAppNameString();
        shareToFacebook(name, link, picture, description, applicationName, getCityName());
    }
}
