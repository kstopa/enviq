package eu.esa.spaceappcamp.enviq.location;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import de.greenrobot.event.EventBus;
import eu.esa.spaceappcamp.enviq.errorhandling.LocationError;

/**
 * Created by hannes on 07/05/14.
 */
public class LocationUtil {

    public static final String LOG_TAG = "EnviQ";
    public static final int TIMEOUT = 15000;


    private static void postLocation(Location location){
        if (location == null)
        {
            EventBus.getDefault().post(new LocationError());
            return;
        }
        EventBus.getDefault().post(location);
    }

    public static Location getLastKnownLocation(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_COARSE);
        String provider = locationManager.getBestProvider(criteria, true);
        if (provider == null) {
           return null;
        }
        return locationManager.getLastKnownLocation(provider);
    }

    public static Location getMockLocation() {
        Location location;
        Log.e(LOG_TAG, "using mock location");
        location = new Location("");
        location.setLatitude(52.366667);
        location.setLongitude(4.9);
        return location;

    }

    public static void querySingleLocation(Context context) {
        querySingleLocation(context, new LocationListener(context));
    }

    public static void querySingleLocation(Context context, android.location.LocationListener listener) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_COARSE);
        String provider = locationManager.getBestProvider(criteria, true);
        if (provider == null) {
            postLocation(null);
            return;
        }

        if (listener instanceof LocationListener)
        {
            ((LocationListener) listener).startTimeOut(TIMEOUT);
        }
        locationManager.requestSingleUpdate(provider, listener, context.getMainLooper());

    }



static class LocationListener implements android.location.LocationListener {

        private final Context context;
        private boolean success = false;



        LocationListener(Context co) {
            context=co.getApplicationContext();
        }

        public void startTimeOut(long timeout)
        {
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    unRegister();
                    sendDefaultIfUnsuccessful();
                }
            },timeout);
        }

        private synchronized void sendDefaultIfUnsuccessful()
        {
            if (!this.success) {
                onLocationChanged(LocationUtil.getLastKnownLocation(context));
            }
        }

        private synchronized void unRegister()
        {  LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            try {
                manager.removeUpdates(LocationListener.this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public synchronized void onLocationChanged(Location location) {
            this.success = true; postLocation(location);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            //dont care
        }

        @Override
        public void onProviderEnabled(String provider) {
            //dont care
        }

        @Override
        public void onProviderDisabled(String provider) {
            //dont care
        }
    }
}
