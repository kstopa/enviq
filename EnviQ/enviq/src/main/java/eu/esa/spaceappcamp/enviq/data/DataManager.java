package eu.esa.spaceappcamp.enviq.data;

/**
 * Caches and handles all data to be get from service that is needed by the app
 * avoiding to download same data twice.
 *
 * Created by kstopa on 07.05.14.
 *
 *
 */
public class DataManager implements DataSource {

    private AsciiGrid pm25;
    private AsciiGrid pm10;
    private AsciiGrid co;
    private AsciiGrid o3;
    private AsciiGrid no2;
    private AsciiGrid so2;
    private DataDownloader ddown;
    private static final DataManager instance = new DataManager();

    public static DataManager getInstance() {
        return instance;
    }

    /**
     * Constructor that generates empty data ascii grids.
     */
    private DataManager() {
        ddown = new DataDownloader();
        pm25 = new AsciiGrid();
        pm10 = new AsciiGrid();
        co = new AsciiGrid();
        o3 = new AsciiGrid();
        no2 = new AsciiGrid();
        so2 = new AsciiGrid();
    }

    /**
     * Gets tiny particles data (PM 2.5) for given area. Downloads the data if needed.
     * @return AsciiGrid with data for given location.
     */
    public AsciiGrid getPM25(double minlat, double minlon, double maxlat, double maxlon) {
        if (!(pm25.contains(minlat, minlon)) ||
            !(pm25.contains(maxlat, maxlon))) {
            pm25 = ddown.getIndex(minlat, minlon, maxlat, maxlon, new Layer(Pollutant.PM2P5));
            return pm25;
        } else {
            return pm25.getData(minlat, minlon, maxlat, maxlon);
        }
    }

    public AsciiGrid getPM10(double minlat, double minlon, double maxlat, double maxlon) {
        if (!(pm10.contains(minlat, minlon)) ||
            !(pm10.contains(maxlat, maxlon))) {
            pm10 = ddown.getIndex(minlat, minlon, maxlat, maxlon, new Layer(Pollutant.PM10));
            return pm10;
        } else {
            return pm10.getData(minlat, minlon, maxlat, maxlon);
        }
    }

    public AsciiGrid getOzone(double minlat, double minlon, double maxlat, double maxlon) {
        if (!(o3.contains(minlat, minlon)) ||
            !(o3.contains(maxlat, maxlon))) {
            o3 = ddown.getIndex(minlat, minlon, maxlat, maxlon, new Layer(Pollutant.O3));
            return o3;
        } else {
            return o3.getData(minlat, minlon, maxlat, maxlon);
        }
    }

    public AsciiGrid getCarbonMonoxide(double minlat, double minlon, double maxlat, double maxlon) {
        if (!(co.contains(minlat, minlon)) ||
            !(co.contains(maxlat, maxlon))) {
            co = ddown.getIndex(minlat, minlon, maxlat, maxlon, new Layer(Pollutant.CO));
            return co;
        } else {
            return co.getData(minlat, minlon, maxlat, maxlon);
        }
    }

    public AsciiGrid getSulphurDioxide(double minlat, double minlon, double maxlat, double maxlon) {
        if (!(so2.contains(minlat, minlon)) ||
            !(so2.contains(maxlat, maxlon))) {
            so2 = ddown.getIndex(minlat, minlon, maxlat, maxlon, new Layer(Pollutant.SO2));
            return so2;
        } else {
            return so2.getData(minlat, minlon, maxlat, maxlon);
        }
    }

    public AsciiGrid getNitrogenDioxide(double minlat, double minlon, double maxlat, double maxlon) {
        if (!(no2.contains(minlat, minlon)) ||
            !(no2.contains(maxlat, maxlon))) {
            no2 = ddown.getIndex(minlat, minlon, maxlat, maxlon, new Layer(Pollutant.NO2));
            return no2;
        } else {
            return no2.getData(minlat, minlon, maxlat, maxlon);
        }
    }


    public double getWeightedPM25(double lat, double lon) {
        if (!pm25.contains(lat, lon)) {
            // Download the data.
            pm25 = ddown.getIndex(lat, lon, new Layer(Pollutant.PM2P5));
        }
        if (isEmptyOrNull(pm25)) {
            // error getting the data.
            return -999.0;  // no data.
        } else {
            return pm25.getWeightedData(lat, lon);
        }
    }

    public double getWeightedPM10(double lat, double lon) throws NoDataException {
        if (!pm10.contains(lat, lon)) {
            // Download the data.
            pm10 = ddown.getIndex(lat, lon, new Layer(Pollutant.PM10));
        }
        if (isEmptyOrNull(pm10)) {
            // error getting the data.
            throw new NoDataException();
        } else {
            return pm10.getWeightedData(lat, lon);
        }
    }

    public double getWeightedOzone(double lat, double lon) throws NoDataException {
        if (!o3.contains(lat, lon)) {
            o3 = ddown.getIndex(lat, lon, new Layer(Pollutant.O3));
        }
        if (isEmptyOrNull(o3)) throw new NoDataException();
        else return o3.getWeightedData(lat, lon);
    }

    public double getWeightedCarbonMonoxide(double lat, double lon) throws NoDataException {
        if (!co.contains(lat, lon)) {
            co = ddown.getIndex(lat, lon, new Layer(Pollutant.CO));
        }
        if (isEmptyOrNull(co)) throw new NoDataException();
        else return co.getWeightedData(lat, lon);
    }

    public double getWeightedNitrogenDioxide(double lat, double lon) throws NoDataException {
        if (!no2.contains(lat, lon)) {
            no2 = ddown.getIndex(lat, lon, new Layer(Pollutant.NO2));
        }
        if (isEmptyOrNull(no2)) throw new NoDataException();
        else return no2.getWeightedData(lat, lon);
    }


    public double getWeightedSulphurDioxide(double lat, double lon) throws NoDataException {
        if (!so2.contains(lat, lon)) {
            so2 = ddown.getIndex(lat, lon, new Layer(Pollutant.SO2));
        }
        if (isEmptyOrNull(so2)) throw new NoDataException();
        else return so2.getWeightedData(lat, lon);
    }

    private boolean isEmptyOrNull(AsciiGrid grid)
    {
        if (grid == null)
        {
            return true;
        }
        return grid.isEmpty();
    }
}
