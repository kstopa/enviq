package eu.esa.spaceappcamp.enviq.graph;

import android.animation.ValueAnimator;
import android.view.animation.AccelerateDecelerateInterpolator;

import eu.esa.spaceappcamp.enviq.models.MeasurementParameter;

/**
 * Created by hannes on 06/05/14.
 */
public class MeasurementParameterAnimator extends ValueAnimator {
    private final MeasurementParameter parameter;

    public MeasurementParameterAnimator(MeasurementParameter parameter) {
        super();
        this.parameter = parameter;
        setIntValues(0, 100);
        setDuration(1500);
        setInterpolator(new AccelerateDecelerateInterpolator());
    }

    public MeasurementParameter getParameter() {
        return parameter;
    }

}
