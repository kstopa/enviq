package eu.esa.spaceappcamp.enviq.data;

import android.util.Log;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by kstopa on 07.05.14.
 */
public class DataDownloader {

    final double RESX = 0.1;

    /**
     * Gets 3x3 ascii grid with data for a location and its surrounding for
     * yesterday at 23 (last available).
     * @param lat Latitude.
     * @param lon Longitude.
     * @return Ascii Grid with data for given location and its surrounding.
     */
    public AsciiGrid getIndex(double lat, double lon, Layer layer) {
        // Calculate needed area.
        double minlat = Math.floor((lat - RESX)*10)/10.0;
        double minlon = Math.floor((lon - RESX)*10)/10.0;
        double maxlat = minlat + RESX*3;//Math.ceil((lat + RESX)*10)/10.0;
        double maxlon = minlon + RESX*3;//Math.ceil((lon + RESX)*10)/10.0;

        return  getIndex(minlat, minlon, maxlat, maxlon, layer);
    }

    /**
     * Gets ascii grid with data for given area for the next available forecasted
     * date for today.
     * @param minlat
     * @param minlon
     * @param maxlat
     * @param maxlon
     * @param layer
     * @todo Improve for get data forecast for selected day.
     * @return
     */
    public AsciiGrid getIndex(double minlat, double minlon, double maxlat, double maxlon, Layer layer) {
        ServiceWCS macc = new ServiceWCS();
        // Service always download a portion (or whole europe)
        AsciiGrid ag = macc.getAsciiData(UrlGenerator.getWcsResource(layer.getName(), minlat, minlon, maxlat, maxlon, layer.getForecast().getDate())); // Replace with resource for given location now gets image for whole Europe
        return ag;
    }
}
