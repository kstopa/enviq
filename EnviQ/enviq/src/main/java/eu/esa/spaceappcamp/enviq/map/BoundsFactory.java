package eu.esa.spaceappcamp.enviq.map;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

/**
 * Created by adamW on 08.05.14.
 */
public class BoundsFactory {

    public static LatLngBounds createBufferArea(Double bufferLat, Double bufferLon, LatLng center) {

        LatLng NE = new LatLng(center.latitude + bufferLat, center.longitude + bufferLon);
        LatLng SW = new LatLng(center.latitude - bufferLat, center.longitude - bufferLon);
        return new LatLngBounds(SW, NE);

    }

}
