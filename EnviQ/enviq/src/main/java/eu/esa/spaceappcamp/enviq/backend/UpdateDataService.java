package eu.esa.spaceappcamp.enviq.backend;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.util.Log;

import de.greenrobot.event.EventBus;
import eu.esa.spaceappcamp.enviq.calculators.IndexCalculator;
import eu.esa.spaceappcamp.enviq.data.DataManager;
import eu.esa.spaceappcamp.enviq.data.DataSource;
import eu.esa.spaceappcamp.enviq.data.NoDataException;
import eu.esa.spaceappcamp.enviq.models.CompleteMeasurementData;
import eu.esa.spaceappcamp.enviq.models.HealthIndex;
import eu.esa.spaceappcamp.enviq.models.Measurement;
import eu.esa.spaceappcamp.enviq.models.MeasurementParameter;
import eu.esa.spaceappcamp.enviq.models.MeasurementParameterFactory;
import eu.esa.spaceappcamp.enviq.utils.ClearEvent;

public class UpdateDataService extends IntentService {
    private static final String LOCATION = "LOCATION";
    private static final String MEASURE_DATA = "MEASURE_DATA";
    private final EventBus eventBus = EventBus.getDefault();
    private DataSource dataManager;
    private Location location;
    private IndexCalculator calculator;

    public static void updateData(Context context, Location location) {
        updateData(context, null, location);
    }

    public static void updateData(Context context, Measurement measurement, Location location) {
        Intent intent = new Intent(context, UpdateDataService.class);
        intent.putExtra(LOCATION, location);
        if (measurement != null) {
            intent.putExtra(MEASURE_DATA, measurement);
        }
        context.startService(intent);
    }


    public UpdateDataService() {
        super(UpdateDataService.class.getName());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        calculator = IndexCalculator.getInstance(getApplicationContext());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
//        Measurement measurement = new Measurement();
//        measurement.setCarbonMonoxide(1);
//        measurement.setFineParticles(1);
//        measurement.setGroundLevelOzone(2);
//        measurement.setNitrogenDioxide(5);
//        measurement.setRespirableSuspendedParticle(1);
//        measurement.setSulfurDioxide(1);
//        dataManager = new FakeDataSource(measurement);
        if (intent.hasExtra(MEASURE_DATA)) {
            dataManager = new FakeDataSource((Measurement) intent.getParcelableExtra(MEASURE_DATA));
        } else {
            dataManager = DataManager.getInstance();
        }
        location = intent.getParcelableExtra(LOCATION);

        try {
            getValuesAndNotifyAfterwards();
        } catch (NoDataException e) {
            e.printStackTrace();
            EventBus.getDefault().post(HealthIndex.getErrorIndex(this));
        }
    }

    private void getValuesAndNotifyAfterwards() throws NoDataException {
        Measurement values = new Measurement();
        CompleteMeasurementData collected = new CompleteMeasurementData();
        collected.setLocation(location);
        notify(new ClearEvent());// clear UI

        if (calculator.needsNitrogenDioxide()) {
            notify(getNitrogenDioxide(values, collected));
        }
        if (calculator.needsGroundLevelOzone()) {
            notify(getGroundLevelOzon(values, collected));
        }
        if (calculator.needsSulfurDioxide()) {
            notify(getSulphurDioxide(values, collected));
        }
        if (calculator.needsRespirableSuspendedParticle()) {
            notify(getParticles(values, collected));
        }
        if (calculator.needsCarbonMonoxide()) {
            notify(getCarbonMonoxide(values, collected));
        }
        if (calculator.needsFineParticles()) {
            notify(getFineParticles(values, collected));
        }
        HealthIndex index= calculateIndex(values, collected);
        notify(index);
        notify(collected);
    }

    private HealthIndex calculateIndex(Measurement values, CompleteMeasurementData collected) {
        int index = calculator.calculate(values);
        HealthIndex healthIndex = new HealthIndex(index, calculator.getAsString(index), calculator.getAsLongString(index));
        collected.set(healthIndex);
        return healthIndex;
    }

    private MeasurementParameter getFineParticles(Measurement values, CompleteMeasurementData collected) throws NoDataException {
        double value = dataManager.getWeightedPM25(location.getLatitude(), location.getLongitude());
        Log.d("ENVIQ", "Value PM25 = " + value);
        MeasurementParameter parameter = MeasurementParameterFactory.createFineParticlesParameter(
                (float) value,
                calculator.getMaxForFineParticles());
        values.setFineParticles(parameter.getValue());
        collected.add(parameter);
        return parameter;
    }

    private MeasurementParameter getCarbonMonoxide(Measurement values, CompleteMeasurementData collected) throws NoDataException {
	double value = dataManager.getWeightedCarbonMonoxide(location.getLatitude(), location.getLongitude());
        Log.d("ENVIQ", "Value CO = " + value);        
        MeasurementParameter parameter = MeasurementParameterFactory.createCarbonMonoxideParameter((float) value,
                calculator.getMaxForCarbonMonoxide());
        values.setCarbonMonoxide(parameter.getValue());
        collected.add(parameter);
        return parameter;
    }

    private MeasurementParameter getParticles(Measurement values, CompleteMeasurementData collected) throws NoDataException {
	double value = dataManager.getWeightedPM10(location.getLatitude(), location.getLongitude());
        Log.d("ENVIQ", "Value PM10 = " + value);
        MeasurementParameter parameter = MeasurementParameterFactory.createRespirableSuspendedParticleParameter(
                (float) value, calculator.getMaxForRespirableSuspendedParticle());
        values.setRespirableSuspendedParticle(parameter.getValue());
        collected.add(parameter);
        return parameter;
    }

    private MeasurementParameter getSulphurDioxide(Measurement values, CompleteMeasurementData collected) throws NoDataException {
        double value = dataManager.getWeightedSulphurDioxide(location.getLatitude(), location.getLongitude());
        Log.d("ENVIQ", "Value SO2 = " + value);
        MeasurementParameter parameter = MeasurementParameterFactory.createSulfurDioxideParameter((float) value, calculator.getMaxForSulfurDioxide());
        values.setSulfurDioxide(parameter.getValue());
        collected.add(parameter);
        return parameter;
    }

    private MeasurementParameter getGroundLevelOzon(Measurement values, CompleteMeasurementData collected) throws NoDataException {
        double value = dataManager.getWeightedOzone(location.getLatitude(), location.getLongitude());
        Log.d("ENVIQ", "Value O3 = " + value);
        MeasurementParameter parameter = MeasurementParameterFactory.createGroundLevelOzoneParameter((float) value, calculator.getMaxForGroundLevelOzone());
        values.setGroundLevelOzone(parameter.getValue());
        collected.add(parameter);
        return parameter;
    }

    private MeasurementParameter getNitrogenDioxide(Measurement values, CompleteMeasurementData collected) throws NoDataException {
        double value = dataManager.getWeightedNitrogenDioxide(location.getLatitude(), location.getLongitude());
        Log.d("ENVIQ", "Value NO2 = " + value);
        MeasurementParameter parameter = MeasurementParameterFactory.createNitrogenDioxideParameter((float) value, calculator.getMaxForNitrogenDioxide());
        values.setNitrogenDioxide(parameter.getValue());
        collected.add(parameter);
        return parameter;
    }

    private void notify(Object object) {
        eventBus.post(object);
    }
}
