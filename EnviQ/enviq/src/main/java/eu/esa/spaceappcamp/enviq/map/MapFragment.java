package eu.esa.spaceappcamp.enviq.map;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.GroundOverlay;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import eu.esa.spaceappcamp.enviq.location.LocationUtil;

/**
 * Created by adamW on 09.05.14.
 */
public class MapFragment extends com.google.android.gms.maps.MapFragment implements GoogleMap.OnCameraChangeListener {

  //  private LocationManager myLocationManager;

    private OverlayManager myOverlayManager;
/*
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        setUpMapIfNeeded();
        focusOnLocation();
        //focusOnLocationAndDraw(); // Move this to another function
        return view;
    }
*/
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setUpMapIfNeeded();
    //    myLocationManager = new LocationUtil();// getActivity().getApplicationContext() );
        myOverlayManager = new OverlayManager();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        view.setBackgroundColor(Color.BLACK);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setUpMapIfNeeded();
        //focusOnLocationAndDraw();
    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        focusOnLocationAndDraw();
    }

    public void setUpMapIfNeeded() {
        if (getMap() != null) {
            getMap().getUiSettings().setAllGesturesEnabled(false);
            getMap().getUiSettings().setZoomControlsEnabled(false);
            getMap().setOnCameraChangeListener(this);
        }

    }

    public void focusOnLocation() {

        Location loc = LocationUtil.getLastKnownLocation(getActivity().getApplicationContext());
        if (loc != null) {
            LatLng myLocation = new LatLng(loc.getLatitude(), loc.getLongitude());
            CameraUpdate up = CameraUpdateFactory.newLatLngZoom(myLocation, 13);
            getMap().moveCamera(up);
        }
    }

    public void focusOnLocationAndDraw() {
        focusOnLocation();
        drawOverlay();
        drawOverlay();
        drawMarker();
    }

    // Default: draw a marker in the center (user location).
    public void drawMarker() {
        drawMarker( getCenterPoint() );
    }

    public void drawMarker(LatLng markerPoint) {

        MarkerOptions myLocationMarker = new MarkerOptions();
        myLocationMarker.position(markerPoint);
        getMap().addMarker(myLocationMarker);
    }

    public LatLng getCenterPoint() {
        CameraPosition cam = getMap().getCameraPosition();
        return new LatLng( cam.target.latitude, cam.target.longitude );
    }

    public void drawOverlay() {
        drawOverlay(null);
    }

    public void drawOverlay(LatLngBounds bounds) {

        try {
            // Draw overlay over whole map.
            if (bounds == null) {
                bounds = getMap().getProjection(). getVisibleRegion().latLngBounds;
            }

            //myOverlayManager.createDefaultOverlay();
            //Bitmap imageBitmap = myOverlayManager.getOverlay();
            Bitmap imageBitmap = myOverlayManager.loadFromDataManager(bounds, ((MapActivity) getActivity()).getSelectedParameter());

            BitmapDescriptor image = BitmapDescriptorFactory.fromBitmap(imageBitmap);

            LatLngBounds bufferedBounds = BoundsFactory.createBufferArea(1.0, 1.0, bounds.getCenter());

            GroundOverlayOptions overlay = new GroundOverlayOptions()
                    .image(image)
                    .anchor(0.5f, 0.5f)
                    .positionFromBounds(bounds)
                    .transparency(0.35f);

            if (imageBitmap.getWidth() == 1 && imageBitmap.getWidth() == 1) {
                overlay.transparency(1);
            }
            getMap().clear();
            getMap().addGroundOverlay( overlay );

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
