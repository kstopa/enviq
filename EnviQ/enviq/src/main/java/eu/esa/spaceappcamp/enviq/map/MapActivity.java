package eu.esa.spaceappcamp.enviq.map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import eu.esa.spaceappcamp.enviq.R;
import eu.esa.spaceappcamp.enviq.models.MeasurementParameter;

/**
 * Created by adamW on 09.05.14.
 */

// This is useless.

public class MapActivity extends Activity implements AdapterView.OnItemSelectedListener {

    static final String KEY_PARAMETERS = "Parameters";
    @InjectView(R.id.changeLayer) Spinner spinner;
    private  ArrayList<MeasurementParameter> parameters;

    public static void start(Context context, ArrayList<MeasurementParameter> parameters) {
        Intent intent = new Intent(context, MapActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(KEY_PARAMETERS, parameters);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        parameters = (ArrayList<MeasurementParameter>) getIntent().getSerializableExtra(KEY_PARAMETERS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_fragment);
        ButterKnife.inject(this);
        spinner.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, getAsStringList(parameters)));
        spinner.setOnItemSelectedListener(this);
        setupMap();
    }

    private void setupMap() {
        MapFragment myMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map));
        myMap.setUpMapIfNeeded();
    }

    private List<String> getAsStringList(ArrayList<MeasurementParameter> parameters) {
        List<String> list = new ArrayList<String>(parameters.size());
        for(MeasurementParameter parameter : parameters)  {
            list.add(parameter.getTitle());
        }
        return list;
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        MapFragment myMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map));
        myMap.drawOverlay();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public MeasurementParameter getSelectedParameter() {
        if (spinner == null) {
            return parameters.get(0);
        }
        return parameters.get(spinner.getSelectedItemPosition());
    }
}
