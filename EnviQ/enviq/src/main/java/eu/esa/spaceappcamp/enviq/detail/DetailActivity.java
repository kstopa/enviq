package eu.esa.spaceappcamp.enviq.detail;

import java.util.ArrayList;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.location.Address;
import android.os.Bundle;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import eu.esa.spaceappcamp.enviq.R;
import eu.esa.spaceappcamp.enviq.map.MapActivity;
import eu.esa.spaceappcamp.enviq.models.HealthIndex;
import eu.esa.spaceappcamp.enviq.models.MeasurementParameter;
import eu.esa.spaceappcamp.enviq.utils.FadeInActivity;


public class DetailActivity extends FadeInActivity {

    private static final int NUM_PAGES = 1;

    static final String KEY_PARAMETERS = "Parameters";
    static final String KEY_ADDRESS = "Address";
    static final String KEY_HEALTHINDEX = "healthIndex";
    private PagerAdapter mPagerAdapter;
    @InjectView(R.id.pager) ViewPager mPager;

    public static void start(Activity context, ArrayList<MeasurementParameter> parameters, Address address, HealthIndex index) {
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra(KEY_PARAMETERS, parameters);
        intent.putExtra(KEY_ADDRESS, address);
        intent.putExtra(KEY_HEALTHINDEX, index);
        startSilent(context, intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.inject(this);
        mPagerAdapter = new ScreenSlidePagerAdapter(getFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        mPager.setCurrentItem(1);


    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
            case 0:
            default:
                return new DetailFragment();
            }
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }
}
