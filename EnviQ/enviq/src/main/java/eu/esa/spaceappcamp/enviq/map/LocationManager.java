package eu.esa.spaceappcamp.enviq.map;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by adamW on 08.05.14.
 */
public class LocationManager implements
        ConnectionCallbacks,
        OnConnectionFailedListener,
        LocationListener {

    private LocationClient GMSLocationClient;
    private int source;
    private Context myContext;
    private LatLng myLastLocation;

    public LocationManager(Context context) {
        myContext = context;
        setUpLocationClientIfNeeded();
        GMSLocationClient.connect();

        myLastLocation = new LatLng(52,21);
    }

    public LatLng getMyLocation() {

        // Choose location source and grab the location

        if (GMSLocationClient != null && GMSLocationClient.isConnected()) {

            GMSLocationClient.requestLocationUpdates(
                    REQUEST,
                    this);  // LocationListener


        }

        return myLastLocation;
    }

    // Demo values
    private static final LocationRequest REQUEST = LocationRequest.create()
            .setInterval(5000)         // 5 seconds
            .setFastestInterval(16)    // 16ms = 60fps
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

    @Override
    public void onConnected(Bundle bundle) {
        //Toast.makeText(myContext, "Connected!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDisconnected() {

    }

    @Override
    public void onLocationChanged(Location location) {
        myLastLocation = new LatLng(location.getLatitude(), location.getLongitude());
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    private void setUpLocationClientIfNeeded() {
        if (GMSLocationClient == null) {
            GMSLocationClient = new LocationClient(
                    myContext,
                    this,  // ConnectionCallbacks
                    this); // OnConnectionFailedListener
        }
    }

}