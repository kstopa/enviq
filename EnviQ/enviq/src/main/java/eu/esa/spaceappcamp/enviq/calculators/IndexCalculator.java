package eu.esa.spaceappcamp.enviq.calculators;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import eu.esa.spaceappcamp.enviq.R;
import eu.esa.spaceappcamp.enviq.models.Measurement;

public abstract class IndexCalculator {

    private final String name;

    protected IndexCalculator(String name) {
        this.name = name;
    }

    public static IndexCalculator getInstance(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String vienna = context.getString(R.string.preference_value_vienna);
        String canada = context.getString(R.string.preference_value_canada);
        String luqx = context.getString(R.string.preference_value_luqx);
        String caqi = context.getString(R.string.preference_value_caqi);
        String key = context.getString(R.string.preference_calculator);
        String index = preferences.getString(key, "");
        if (index.equals(vienna)) {
            return new ViennaAirIndex(context);
        } else if (index.equals(luqx)) {
            return new BadenWuertembergAirIndex(context);
        } else if (index.equals(canada)) {
            return new CanadianHealthIndex(context);
        } else if (index.equals(caqi)) {
            return new EuropeanCommonAirQualityIndex(context);
        }

        // nothing set?
        preferences.edit().putString(key, caqi).commit();
        return new EuropeanCommonAirQualityIndex(context);
    }

    public abstract int calculate(Measurement measurement);

    public abstract String getAsString(int index);
    public abstract String getAsLongString(int index);

    public abstract boolean needsNitrogenDioxide();
    public abstract boolean needsGroundLevelOzone();
    public abstract boolean needsFineParticles();
    public abstract boolean needsRespirableSuspendedParticle();
    public abstract boolean needsSulfurDioxide();
    public abstract boolean needsCarbonMonoxide();

    public abstract float getMaxForNitrogenDioxide();
    public abstract float getMaxForGroundLevelOzone();
    public abstract float getMaxForFineParticles();
    public abstract float getMaxForRespirableSuspendedParticle();
    public abstract float getMaxForSulfurDioxide();
    public abstract float getMaxForCarbonMonoxide();

    public abstract int getMaxIndex();

    public String getName() {
        return name;
    }
}
