package eu.esa.spaceappcamp.enviq.data;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.concurrent.ExecutionException;


/**
 * Created by kstopa on 06.05.14.
 */
public class ServiceWCS {

    public ServiceWCS() {
        //
    }


    public AsciiGrid getAsciiData(String url) {
        Log.d("ENVIQ", url);
        AsciiGrid dt;
        try {
            dt = new ServerDataTask().execute(url).get();
        } catch (InterruptedException e) {
            dt = new AsciiGrid();
        } catch (ExecutionException e) {
            dt = new AsciiGrid();
        }
        return dt;
    }

    /**
     * Run async task that get asii grid data based on given HTTP request.
     *
     * @author kstopa
     */
    class ServerDataTask extends AsyncTask<String, String, AsciiGrid>{
        @Override
        protected AsciiGrid doInBackground(String... url) {
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet httpRequest = new HttpGet(url[0]);
            try {
                HttpResponse httpResponse = client.execute(httpRequest);
                final int statusCode = httpResponse.getStatusLine().getStatusCode();

                if (statusCode != HttpStatus.SC_OK) {
                    return null;
                }
                HttpEntity httpEntity = httpResponse.getEntity();
                BufferedReader buffer = new BufferedReader(new InputStreamReader(httpEntity.getContent()));
                // Read ascii grid header
                /*  ncols        700
                    nrows        400
                    xllcorner    -25.000000000000
                    yllcorner    30.000000000000
                    cellsize     0.100000000000
                    NODATA_value   -999*/
                /**
                 * @todo improve with regex.
                 */
                String headl1 = buffer.readLine();
                if (!headl1.contains("ncols")) {
                    Log.d("ENVIQ", "Error parsing data " + headl1);
                    return new AsciiGrid();
                }
                int ncols = Integer.valueOf(headl1.replace("ncols", "").replace(" ", ""));  // cols
                int nrows = Integer.valueOf(buffer.readLine().replace("nrows", "").replace(" ", ""));  // rows
                double xllcor = Double.valueOf(buffer.readLine().replace("xllcorner", "").replace(" ", ""));  // min lon
                double yllcor = Double.valueOf(buffer.readLine().replace("yllcorner", "").replace(" ", ""));  // min lat
                double cell = Double.valueOf(buffer.readLine().replace("cellsize", "").replace(" ", ""));  // cell size
                double nodata = Double.valueOf(buffer.readLine().replace("NODATA_value", "").replace(" ", ""));  // min lon

                // Create ascii file
                AsciiGrid asciiGrid = new AsciiGrid(nrows, ncols, yllcor, xllcor, cell, nodata);
                Log.d("ENVIQ", "Get ascii grid. Size " + nrows + ", " + ncols);
                // Read each row and col
                for (int nr=0; nr<nrows; nr++) {
                    String line = buffer.readLine();
                    //Log.d("ENVIQ", line);
                    String[] cols = line.trim().split("\\s");
                    // @todo check if same length than number
                    for (int nc=0; nc<cols.length; nc++) {
                        asciiGrid.setData(nr, nc, Double.valueOf(cols[nc]));
                    }
                }
                return asciiGrid;
            } catch (ClientProtocolException e) {
                // return empty grid
                return new AsciiGrid();
            } catch (IOException e) {
                return new AsciiGrid();
            } /*catch (ParseException e) {
                return null;
            }*/
        }

        @Override
        protected void onPostExecute(AsciiGrid ag) {
            /** Run like this: Object result = asyncTask.execute().get(); */
        }
    }

}
