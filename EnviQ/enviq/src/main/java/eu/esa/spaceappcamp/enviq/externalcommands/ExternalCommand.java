package eu.esa.spaceappcamp.enviq.externalcommands;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

import eu.esa.spaceappcamp.enviq.models.Measurement;

public class ExternalCommand implements Parcelable {

    private final Measurement measurement;
    private final Location location;

    public Measurement getMeasurement() {
        return measurement;
    }

    public Location getLocation() {
        return location;
    }

    public ExternalCommand(Measurement measurement, Location location) {
        this.measurement = measurement;
        this.location = location;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.measurement, 0);
        dest.writeParcelable(this.location, 0);
    }

    private ExternalCommand(Parcel in) {
        this.measurement = in.readParcelable(Measurement.class.getClassLoader());
        this.location = in.readParcelable(Location.class.getClassLoader());
    }

    public static Parcelable.Creator<ExternalCommand> CREATOR = new Parcelable.Creator<ExternalCommand>() {
        public ExternalCommand createFromParcel(Parcel source) {
            return new ExternalCommand(source);
        }

        public ExternalCommand[] newArray(int size) {
            return new ExternalCommand[size];
        }
    };
}
