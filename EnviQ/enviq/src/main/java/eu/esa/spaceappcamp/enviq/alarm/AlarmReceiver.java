package eu.esa.spaceappcamp.enviq.alarm;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import eu.esa.spaceappcamp.enviq.EnviQApplication;
import eu.esa.spaceappcamp.enviq.MainActivity;
import eu.esa.spaceappcamp.enviq.utils.NetworkUtil;

public class AlarmReceiver extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(MainActivity.LOG_TAG,"AlarmReceiver - onCreate");
        super.onCreate(savedInstanceState);

        if (NetworkUtil.isOnline(this)) {
            EnviQApplication app = (EnviQApplication) getApplicationContext();
            app.triggerLocation();
        }
        finish();
    }
}
