package eu.esa.spaceappcamp.enviq.utils;


import android.graphics.Color;

/**
 *
 *
 * Created by kstopa on 08.05.14.
 */
public class ColorScale {

    double maxval;

    public ColorScale(double max) {
        maxval = max;
    }

    /**
     *
     * @param val
     * @return
     */
    public int getColor(double val) {
        float hsv[] = {215f - (float)((215f/maxval)*val), 100f, 100f };
        return Color.HSVToColor(hsv);
    }

    /**
     * Gives color scale but with darky colors.
     * @param val
     * @return
     */
    public int getColorDark(double val) {
        double r, g, b;
        // As a percentage.
        double value = val/maxval;
        if (0 <= value && value <= 1/8) {
            r = 0;
            g = 0;
            b = 4*value + .5; // .5 - 1 // b = 1/2
        } else if (1/8 < value && value <= 3/8) {
            r = 0;
            g = 4*value - .5; // 0 - 1 // b = - 1/2
            b = 1; // small fix
        } else if (3/8 < value && value <= 5/8) {
            r = 4*value - 1.5; // 0 - 1 // b = - 3/2
            g = 1;
            b = -4*value + 2.5; // 1 - 0 // b = 5/2
        } else if (5/8 < value && value <= 7/8) {
            r = 1;
            g = -4*value + 3.5; // 1 - 0 // b = 7/2
            b = 0;
        } else if (7/8 < value && value <= 1) {
            r = -4*value + 4.5; // 1 - .5 // b = 9/2
            g = 0;
            b = 0;
        } else {    // should never happen - value > 1
            r = .5;
            g = 0;
            b = 0;
        }

        // scale for hex conversion
        r *= 16 ;
        g *= 16 ;
        b *= 16 ;

        return Color.rgb((int)r, (int)g, (int)b);
    }

}
